// return the user data from the session storage
export const getUser = () => {
  const userStr = localStorage.getItem('user');
  if (userStr) return JSON.parse(userStr);
  else return null;
}

// return the token from the session storage
export const getToken = () => {
  return localStorage.getItem('token') || null;
}

// remove the token and user from the session storage
export const removeUserSession = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
}

// set the token and user from the session storage
export const setUserSession = (token, user) => {
  localStorage.setItem('token', token);
  localStorage.setItem('user', JSON.stringify(user));
}

// Set Scan Data
export const setScanDate = (date) => {
  localStorage.setItem('date', date);
}
export const setScanShift = (shift) => {
  localStorage.setItem('shift', shift);
}
export const setScanTeam = (team) => {
  localStorage.setItem('team', team);
}
export const setScanPackingList = (packing_list) => {
  localStorage.setItem('packing_list', packing_list);
}
export const setScanSlot = (slot) => {
  localStorage.setItem('slot', slot);
}
export const setScanId = (scanId) => {
  localStorage.setItem('scanId', scanId);
}
export const setDailyShiftTeamId = (dailyShiftTeamId) => {
  localStorage.setItem('dailyShiftTeamId', dailyShiftTeamId);
}

// Remove Scan Data
export const removeScanDate = () => {
  localStorage.removeItem('date');
}
export const removeScanShift = () => {
  localStorage.removeItem('shift');
}
export const removeScanTeam = () => {
  localStorage.removeItem('team');
}
export const removeScanPackingList = () => {
  localStorage.removeItem('packing_list');
}
export const removeScanSlot = () => {
  localStorage.removeItem('slot');
}
export const removeScanId = () => {
  localStorage.removeItem('scanId');
}
export const removeDailyShiftTeamId = () => {
  localStorage.removeItem('dailyShiftTeamId');
}

// Get Scan Data
export const getScanDate = () => {
  return localStorage.getItem('date') || null;
}
export const getScanShift = () => {
  return localStorage.getItem('shift') || null;
}
export const getScanTeam = () => {
  return localStorage.getItem('team') || null;
}
export const getScanPackingList = () => {
  return localStorage.getItem('packing_list') || null;
}
export const getScanSlot = () => {
  return localStorage.getItem('slot') || null;
}
export const getScanId = () => {
  return localStorage.getItem('scanId') || null;
}
export const getDailyShiftTeamId = () => {
  return localStorage.getItem('dailyShiftTeamId') || null;
}