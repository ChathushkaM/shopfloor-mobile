import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getToken, getUser } from './Common';
import Header from '../components/theme/Header';
import Footer from '../components/theme/Footer';

// handle the private routes
function PrivateRoute({ component: Component, theme: Theme, ...rest }) {
  const user = getUser();
  return (
    <Route
      {...rest}
      render={
        (props) => getToken() ?
          <>
            <Theme
              main={Component}
              header={Header}
              footer={Footer}
              {...props}
            />
          </> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      }
    />
  )
}

export default PrivateRoute;