import React, { useState } from 'react';
import { generateSetScanningSlotDisplay } from './SetScanningSlotDS';
import config from './SetScanningSlotCS';
import API from '../../../api/API';
import { setScanDate, setScanShift, setScanTeam,setScanPackingList, setScanSlot, setScanId, setDailyShiftTeamId, getScanId, removeScanDate, removeScanShift, removeScanTeam,removeScanPackingList, removeScanSlot, removeScanId, removeDailyShiftTeamId } from '../../../utils/Common';
import { useHistory } from 'react-router-dom';

const SetScanningSlot = () => {
    const history = useHistory();
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender;

    config["resetButton"].event.onClick = handleResetData;

    config["inputDate"].event.onChange = handleChangeDate;
    config["inputShift"].event.onEnterKey = handleScanShift;
    config["inputTeam"].event.onEnterKey = handleScanTeam;
    config["inputPackingList"].event.onEnterKey = handleScanPackingList;
    config["inputSlot"].event.onEnterKey = handleScanSlot;

    config["okButton"].event.onClick = handleSetScanningSlot;

    config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    config["CONTROL_CENTER"].event.onNew = handleNew;
    config["CONTROL_CENTER"].event.onDelete = handleDelete;
    config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/
    /*********************************************************/

    function handleChange(event) {
        return onChange(event);
    }

    function handlePopulate(event, callback) {
        return onPopulate(event, callback);
    }

    function handleNew(event) {
        return onNew();
    }

    function handleDelete(event) {
        return onDelete();
    }

    function handleRefresh(event) {
        return onRefresh();
    }

    function handleSave(event, beforeSaveArr) {
        if (beforeSaveArr.action === "NEW") {
            onSaveNew(beforeSaveArr)
        } else if (beforeSaveArr.action === "DELETE") {
            onSaveDelete(beforeSaveArr)
        } else if (beforeSaveArr.action === "MODIFY") {
            onSaveModify(beforeSaveArr)
        }
        let afterSaveArr = { ...beforeSaveArr.data }
        return afterSaveArr
    }

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    removeScanDate();
    removeScanShift();
    removeScanTeam();
    removeScanPackingList();
    removeScanSlot();
    removeScanId();
    removeDailyShiftTeamId();

    window.onload = (event) => {
        let today = new Date();
        config["inputDate"].setDate(__formatDateYmd(today));
        document.getElementById("inputShift").focus();
        document.getElementsByClassName("react-datepicker-wrapper")[0].getElementsByTagName("input")[0].readOnly = true;
    };

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    // Get Details
    async function __getDetails(key, distinct, select, where, relations, orderby, limit) {
        try {
            const apiRequest = {
                [key]: {
                    "distinct": distinct,
                    "select": select,
                    "where": where,
                    "relations": relations,
                    "orderby": orderby,
                    "limit": limit
                }
            };

            const getDetails = await API.post(`searchByParameters`, apiRequest);
            const details = getDetails.data;

            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error.response);
            return "Error";
        }
    }

    function __formatDateYmd(date) {
        let convertedDate = new Date(date);
        let formattedDate = "";
        let day = convertedDate.getDate();
        let month = convertedDate.getMonth() + 1;
        let year = convertedDate.getFullYear();
        if (day < 10) { day = `0${day}` }
        if (month < 10) { month = `0${month}` }
        formattedDate = `${year}-${month}-${day}`;
        return formattedDate;
    }

    //Reset form
    function __resetForm() {
        try {
            let value = "";
            config["inputShift"].setValue(value);
            config["inputShiftId"].setValue(value);
            config["inputShiftName"].setValue(value);

            config["inputTeam"].setValue(value);
            config["inputTeamId"].setValue(value);
            config["inputTeamName"].setValue(value);

            config["inputSlot"].setValue(value);
            config["inputSlotId"].setValue(value);
            config["inputSlotName"].setValue(value);

        } catch (error) {
            console.log(error.response)
        }
    }

    async function __getShiftDetails(shiftId) {
        let shiftDetail = "";
        try {
            const key = "Shift";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": shiftId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Shift.length > 0) {
                const details = getDetails[0].Shift[0];
                shiftDetail = details.name;
            }
        } catch (error) {
            console.log(error.response);
        }
        return shiftDetail;
    }

    async function __getTeamDetails(teamId) {
        let teamDetail = "";
        try {
            const key = "Team";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": teamId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Team.length > 0) {
                const details = getDetails[0].Team[0];
                teamDetail = details.description;
            }
        } catch (error) {
            console.log(error.response);
        }
        return teamDetail;
    }

    async function __getPackinglistDetails(packing_list_id) {
        let packing_list_details = "";
        try {
            const key = "PackingList";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": packing_list_id
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].PackingList.length > 0) {
                const details = getDetails[0].PackingList[0];
                packing_list_details = details.id;
            }
        } catch (error) {
            console.log(error.response);
        }
        return packing_list_details;
    }

    async function __getSlotDetails(slotId) {
        let slotDetail = "";
        try {
            const key = "DailyScanningSlot";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "seq_no",
                    "operator": "=",
                    "value": slotId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].DailyScanningSlot.length > 0) {
                const details = getDetails[0].DailyScanningSlot[0];
                slotDetail = details.seq_no;
            }
        } catch (error) {
            console.log(error.response);
        }
        return slotDetail;
    }

    /*********************************************************/
    /********      Framework Public Functions       **********/

    /*********************************************************/

    async function handleResetData() {
        let today = new Date();
        handleChangeDate(today);
    }

    function handleChangeDate(date) {
        config["inputDate"].setDate(__formatDateYmd(date));
        __resetForm();
        document.getElementById("inputShift").focus();
    }

    async function handleScanShift(event) {
        event.preventDefault();
        try {
            const shiftId = config["inputShift"].data.value;
            const shiftDetail = await __getShiftDetails(shiftId);

            if (shiftDetail !== "") {
                config["inputShiftId"].setValue(shiftId);
                config["inputShiftName"].setValue(shiftDetail);
                config["inputShift"].setValue(" ");
                document.getElementById("inputTeam").focus();
            } else {
                config["inputShiftId"].setValue("");
                config["inputShiftName"].setValue("");
                config["inputShift"].setValue(" ");
                document.getElementById("inputShift").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Shift Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanTeam(event) {
        event.preventDefault();
        try {
            const teamId = config["inputTeam"].data.value;
            const teamDetail = await __getTeamDetails(teamId);

            if (teamDetail !== "") {
                config["inputTeamId"].setValue(teamId);
                config["inputTeamName"].setValue(teamDetail);
                config["inputTeam"].setValue(" ");
                document.getElementById("inputPackingList").focus();
            } else {
                config["inputTeamId"].setValue("");
                config["inputTeamName"].setValue("");
                config["inputTeam"].setValue(" ");
                document.getElementById("inputTeam").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Team Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanPackingList(event){
        event.preventDefault();
        try {
            const packingListId = config["inputPackingList"].data.value;
            const packingListDetails = await __getPackinglistDetails(packingListId);

            if (packingListDetails !== "") {
                config["inputPackingListId"].setValue(packingListId);
                config["inputPackingListName"].setValue(packingListDetails);
                config["inputPackingList"].setValue(" ");
                document.getElementById("inputSlot").focus();
            } else {
                config["inputPackingListId"].setValue("");
                config["inputPackingListName"].setValue("");
                config["inputPackingList"].setValue(" ");
                document.getElementById("inputPackingList").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Packing List Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanSlot(event) {
        event.preventDefault();
        try {
            const slotId = config["inputSlot"].data.value;
            const slotDetail = await __getSlotDetails(slotId);

            if (slotDetail !== "") {
                config["inputSlotId"].setValue(slotId);
                config["inputSlotName"].setValue(slotDetail);
                config["inputSlot"].setValue(" ");
                document.getElementsByClassName("main-button")[0].focus();
            } else {
                config["inputSlotId"].setValue("");
                config["inputSlotName"].setValue("");
                config["inputSlot"].setValue(" ");
                document.getElementById("inputSlot").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Slot Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleSetScanningSlot(event) {
        event.preventDefault();
        try {
            const date = config["inputDate"].data.value;
            const shiftId = config["inputShiftId"].data.value;
            const shiftName = config["inputShiftName"].data.value;
            const teamId = config["inputTeamId"].data.value;
            const packing_list_id = config["inputPackingListId"].data.value;
            const teamName = config["inputTeamName"].data.value;
            const slotId = config["inputSlotId"].data.value;
            let dailyScanningSlotId = "";
            let dailyShiftTeamId = "";

            if(date !== "" && shiftId !== "" && teamId !== "" && slotId !== ""){
                const apiRequest = {
                    "current_date": __formatDateYmd(date),
                    "shift_id": shiftId,
                    "team_id": teamId,
                    "seq_no": slotId
                };
                
                document.getElementById("spinner").style.display = "";
    
                const setScanningSlot = await API.post(`dailyScanningSlots/getBySeqNo`, apiRequest);

                console.log(apiRequest);
                console.log(setScanningSlot);

                document.getElementById("spinner").style.display = "none";
    
                if((setScanningSlot.data.DailyScanningSlot !== null) && (setScanningSlot.data.DailyShiftTeam !== null)){
                    console.log('TREEEEE');
                    dailyScanningSlotId = setScanningSlot.data.DailyScanningSlot.id;
                    dailyShiftTeamId = setScanningSlot.data.DailyShiftTeam.id;
                    
                    setScanDate(__formatDateYmd(date));
                    setScanShift(shiftName);
                    setScanTeam(teamName);
                    setScanPackingList(packing_list_id);
                    setScanSlot(slotId);
                    setScanId(dailyScanningSlotId);
                    setDailyShiftTeamId(dailyShiftTeamId);

                    history.push(`/scanBundle`);
                    window.location.reload();
                }else{
                    config["CONTROL_CENTER"].promptWarningMessage("No Data Available", "");
                }

            }else{
                if(date === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Select Date", "");
                }else if(shiftId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Shift", "");
                }else if(teamId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Team", "");
                }else if(slotId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Slot", "");
                }
            }
            
        } catch (error) {
            document.getElementById("spinner").style.display = "none";
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    function onChange(event) {
        event.preventDefault();
        alert("This is the place where you write CHANGE")
    }

    function onPopulate(event) {
        event.preventDefault();
        alert("This is the place where you write POPULATE")
    }

    function onNew() {
        let dataArray = {};
        //Action handling when NEW buttion clicked...
        alert("This is the place where you write NEW")
        return dataArray
    }

    function onDelete() {
        //Action handling when DELETE buttion clicked...
        alert("This is the place where you write DELETE")
    }

    function onRefresh() {
        //Action handling when REFRESH buttion clicked...
        alert("This is the place where you write REFRESH")
    }

    function onSaveNew(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveModify(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveDelete(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    return generateSetScanningSlotDisplay(config)
}

export default SetScanningSlot;