
let componentListConfig = []

componentListConfig["CONTROL_CENTER"] = {
    objectType: "Controller",
    schema: {
        id: "formSetScanningSlot",
        name: "formSetScanningSlot",
        controllerObject: componentListConfig,
        create: true,
        createAPI: "",
        read: true,
        readAPI: "",
        update: true,
        updateAPI: "",
        delete: false,
        deleteAPI: ""
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSetScanningSlot",
            type: "text",
            visible: true,
            value: "Set Scanning Slot"
        },
    },
    state: {
        populated: false,
        modified: false,
        deleted: false,
        new: false
    },
    actions: {
        save: "buttonSave",
        delete: "buttonDelete",
        populate: "buttonPopulate",
        refresh: "buttonRefresh"
    },
    event: {
    }
}

componentListConfig["buttonPopulate"] = {
    objectType: "Button",
    schema: {
        id: "buttonPopulate",
        name: "buttonPopulate",
        type: "submit",
        label: "Populate",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonNew"] = {
    objectType: "Button",
    schema: {
        id: "buttonNew",
        name: "buttonNew",
        type: "submit",
        label: "New",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonSave"] = {
    objectType: "Button",
    schema: {
        id: "buttonSave",
        name: "buttonSave",
        type: "submit",
        label: "Save",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonRefresh"] = {
    objectType: "Button",
    schema: {
        id: "buttonRefresh",
        name: "buttonRefresh",
        type: "submit",
        label: "Clear",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDelete"] = {
    objectType: "Button",
    schema: {
        id: "buttonDelete",
        name: "buttonDelete",
        type: "submit",
        label: "Delete",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputDate"] = {
    objectType: "DateField",
    schema: {
        name: "inputDate",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelInputDate",
            type: "text",
            visible: true,
            value: "Date"
        },
        class: ""
    },
    data: {
        sqlcolumn: "date",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShift"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputShift",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShift",
            type: "text",
            visible: true,
            value: "Shift"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftId",
        placeholder: "Shift Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftId",
            type: "text",
            visible: false,
            value: "Shift Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftName",
            type: "text",
            visible: true,
            value: "Shift Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeam"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputTeam",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeam",
            type: "text",
            visible: true,
            value: "Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamId",
        placeholder: "Team Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamId",
            type: "text",
            visible: false,
            value: "Team Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamName",
            type: "text",
            visible: true,
            value: "Team Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

/////////////////////////////////////////  Packing List  ////////////////////////////////////////////////////////

componentListConfig["inputPackingList"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputPackingList",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingList",
            type: "text",
            visible: true,
            value: "Packing List"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputPackingListId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputPackingListId",
        placeholder: "Packing List Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingListId",
            type: "text",
            visible: false,
            value: "Packing List Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputPackingListName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputPackingListName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingListName",
            type: "text",
            visible: true,
            value: "Packing List Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

/////////////////////////////////////////////////////////////////////////////////////////////////

componentListConfig["inputSlot"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputSlot",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlot",
            type: "text",
            visible: true,
            value: "Slot"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotId",
        placeholder: "Slot Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotId",
            type: "text",
            visible: false,
            value: "Slot Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotName",
            type: "text",
            visible: true,
            value: "Slot Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["okButton"] = {
    objectType: "Button",
    schema: {
        id: "okButton",
        name: "okButton",
        type: "submit",
        label: "OK",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["resetButton"] = {
    objectType: "Button",
    schema: {
        id: "resetButton",
        name: "resetButton",
        type: "submit",
        label: "Reset",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

export default componentListConfig