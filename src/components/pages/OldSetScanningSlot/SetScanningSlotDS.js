import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField, DateField } from '../../../BASE/Components'

export function generateSetScanningSlotDisplay(componentList, control) {

    return (
        <>
            <div className="loading" id="spinner" style={{ display: "none" }}>Loading&#8230;</div>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>

            <div className="container-fluid py-3">
                <div className="form-row">
                    <div className="form-group col-md-12">
                        <Label item={componentList["inputDate"].label} />
                        <DateField item={componentList["inputDate"]} dateFormat="yyyy-MM-dd" />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-12 mb-0">
                        <Label item={componentList["inputShift"].label} />
                        <IntegerField item={componentList["inputShift"]} className="form-control form-control-sm" />
                        <TextBox item={componentList["inputShiftId"]} />
                    </div>
                    <div className="form-group col-md-12">
                        <TextBox item={componentList["inputShiftName"]} className="form-control-plaintext display-text" />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-12 mb-0">
                        <Label item={componentList["inputTeam"].label} />
                        <IntegerField item={componentList["inputTeam"]} className="form-control form-control-sm" />
                        <TextBox item={componentList["inputTeamId"]} />
                    </div>
                    <div className="form-group col-md-12">
                        <TextBox item={componentList["inputTeamName"]} className="form-control-plaintext display-text" />
                    </div>
                </div>
                <div hidden className="form-row">
                    <div className="form-group col-md-12 mb-0">
                        <Label item={componentList["inputPackingList"].label} />
                        <IntegerField item={componentList["inputPackingList"]} className="form-control form-control-sm" />
                        <TextBox item={componentList["inputPackingListId"]} />
                    </div>
                    <div className="form-group col-md-12">
                        <TextBox item={componentList["inputPackingListName"]} className="form-control-plaintext display-text" />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-12 mb-0">
                        <Label item={componentList["inputSlot"].label} />
                        <IntegerField item={componentList["inputSlot"]} className="form-control form-control-sm" />
                        <TextBox item={componentList["inputSlotId"]} />
                    </div>
                    <div className="form-group col-md-12">
                        <TextBox item={componentList["inputSlotName"]} className="form-control-plaintext display-text" />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                        <Button className="btn btn-primary btn-lg main-button" item={componentList["okButton"]}> OK</Button>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                        <Button className="btn btn-link" item={componentList["resetButton"]}> Reset</Button>
                    </div>
                </div>
            </div>
        </>
    )
}
