import React, { useState } from 'react';
import { generateScanBundleDisplay } from './ScanBundleDS';
import config from './ScanBundleCS';
import API from '../../../api/API';
import { getScanDate, getScanShift, getScanTeam,getScanPackingList, getDailyShiftTeamId, getScanSlot, getScanId } from '../../../utils/Common';
import { useHistory } from 'react-router-dom';
import { getUser } from '../../../utils/Common';

const ScanBundle = () => {
    var showWhenChecked1 = false;
    var bundle_id1 = 0;
    var soc_id_nb = 0;
    var operation_nb = "";
    var direction_nb = "";
    var max_quantity_nb = 0;
    var bundle_size_nb;
    var neededBundleTicketData;
    var neededPackingListSOCData;
    const history = useHistory();
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender

    config["inputBundleId"].event.onEnterKey = socValidator;
    config["backButton"].event.onClick = handleBackButton;
    config["newInputButton"].event.onClick = handleNewInputButtonPress;
    config["quantityModifier"].event.onCloseClick = handleQuantityModifierClose
    config["quantityModifier"].event.onQuantityClick = handleQuantityModifierQtyClick
    config["buttonUpdateQuantity"].event.onClick = handleUpdateQuantity;
    config["buttonYesPopUp"].event.onClick = handleDeleteScanRecordPopUp;
    config["buttonNoPopUp"].event.onClick = handleCloseDeleteScanRecordPopUp;
    config["newCheckBox"].event.onClick = handleCheckBoxEvent;
    config["inputSecondPackingList"].event.onChange = handleDropDown;

    config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    config["CONTROL_CENTER"].event.onNew = handleNew;
    config["CONTROL_CENTER"].event.onDelete = handleDelete;
    config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/
    /*********************************************************/

    function handleChange(event) {
        return onChange(event);
    }

    function handlePopulate(event, callback) {
        return onPopulate(event, callback);
    }

    function handleNew(event) {
        return onNew();
    }

    function handleDelete(event) {
        return onDelete();
    }

    function handleRefresh(event) {
        return onRefresh();
        
    }

    function handleCheckBoxEvent(event){
        const x = event.target.checked;
        showWhenChecked1 = x;
        console.log(showWhenChecked1 , "Check Box Value");
        if(x){
            document.getElementById("showWhenChecked").hidden = false;
        }
        else{
            document.getElementById("showWhenChecked").hidden = true;
        }
    }

    function handleDropDown(event){
        console.log(event.target.value , "DROP DOWN VALUE");
        config["inputScanPackingList"].setValue(event.target.value);
    }

    function handleSave(event, beforeSaveArr) {
        if (beforeSaveArr.action === "NEW") {
            onSaveNew(beforeSaveArr)
        }
        else if (beforeSaveArr.action === "DELETE") {
            onSaveDelete(beforeSaveArr)
        }
        else if (beforeSaveArr.action === "MODIFY") {
            onSaveModify(beforeSaveArr)
        }
        let afterSaveArr = { ...beforeSaveArr.data }
        return afterSaveArr
    }

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    const scanDate = getScanDate();
    const scanShift = getScanShift();
    const scanTeam = getScanTeam();
    const scanSlot = getScanSlot();
    const scanId = getScanId();
    const packing_list = getScanPackingList();
    const dailyShiftTeamId = getDailyShiftTeamId();

    if (scanId === null) {
        history.push(`/setScanningSlot`);
        window.location.reload();
    }

    __setScanningList(scanId, dailyShiftTeamId); 

    window.onload = (event) => {
        
        config["inputScanDate"].setValue(scanDate);
        config["inputScanShift"].setValue(scanShift);
        config["inputScanTeam"].setValue(scanTeam);
        config["inputScanSlot"].setValue(scanSlot);
        config["inputScanPackingList"].setValue(packing_list);
        config["inputScanId"].setValue(scanId);
        config["inputDailyShiftTeamId"].setValue(dailyShiftTeamId);
        document.getElementById("inputBundleId").focus();
        document.getElementById("showWhenChecked").hidden = true;
        // config["newCheckBox"].setDisabled(true);
    };

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    // Get Details
    async function __getDetails(key, distinct, select, where, relations, orderby, limit) {
        try {
            const apiRequest = {
                [key]: {
                    "distinct": distinct,
                    "select": select,
                    "where": where,
                    "relations": relations,
                    "orderby": orderby,
                    "limit": limit
                }
            };

            const getDetails = await API.post(`searchByParameters`, apiRequest);
            const details = getDetails.data;

            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error.response);
            return "Error";
        }
    }

    async function __setScanningList(scanningSlotId, dailyShiftTeamId) {
        let list = [];
        const user = getUser();
        try {
            const key = "BundleTicket";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "daily_scanning_slot_id",
                    "operator": "=",
                    "value": scanningSlotId
                },
                {
                    "field-name": "daily_shift_team_id",
                    "operator": "=",
                    "value": dailyShiftTeamId
                }// },
                // {
                //     "field-name": "created_by",
                //     "operator": "=",
                //     "value": user.name
                // }
            ];
            const relations = [
                "fpo_operation"
            ];
            const orderby = "scan_date_time:desc";
            const limit = 1000;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            
            if (getDetails && getDetails !== "Error" && getDetails[0].BundleTicket.length > 0) {
                getDetails[0].BundleTicket.forEach(data => {
                    list.push({ 
                        id: data.id, 
                        scanId: data.id,
                        quantity: data.scan_quantity, 
                        fullQuantity: data.original_quantity,
                        displayText: data.fpo_operation.description
                    });
                });
            }
        } catch (error) {
            console.log(error.response);
        }

        config["quantityModifier"].setList(list);
    }

    async function __getScanQuantity(bundleTicketId) {
        let quantity = "";
        try {
            const key = "BundleTicket";
            const distinct = false;
            const select = ["scan_quantity"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": bundleTicketId
                }
            ];
            const relations = [];
            const orderby = "created_at:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            
            if (getDetails && getDetails !== "Error" && getDetails[0].BundleTicket.length > 0) {
                quantity = getDetails[0].BundleTicket[0].scan_quantity;
            }
        } catch (error) {
            console.log(error.response);
        }

        return quantity;
    }

    /*********************************************************/
    /********      Framework Public Functions       **********/
    /*********************************************************/

    async function handleQuantityModifierClose(event, id){
        if(id !== ""){
            config["deleteScanRecordPopUp"].showPopUp();
            config["inputDeleteBundleTicketId"].data.value = id;
        }
    }

    async function handleDeleteScanRecordPopUp(){
        const user = getUser();
        try {
            const bundleTicketId = config["inputDeleteBundleTicketId"].data.value;
            const dailyScanningSlotId = config["inputScanId"].data.value;
            const dailyShiftTeamId = config["inputDailyShiftTeamId"].data.value;

            if(bundleTicketId !== "" && dailyScanningSlotId !== ""){

                config["deleteScanRecordPopUp"].closePopUp();

                const apiRequest = {
                    "bundle_ticket_id": bundleTicketId,
                    "username": user.name
                };

                document.getElementById("spinner").style.display = "";

                const unscanBundleTicket = await API.post(`bundleTickets/unscan`, apiRequest);

                document.getElementById("spinner").style.display = "none";

                if (unscanBundleTicket.data.status === "success") {
                    config["CONTROL_CENTER"].promptBaseMessage("Record Deleted Successfully", "");
                    await __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                } else {
                    config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                }

            }else{
                config["CONTROL_CENTER"].promptWarningMessage("Error", "");
            }
            
        } catch (error) {
            document.getElementById("spinner").style.display = "none";
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    function handleCloseDeleteScanRecordPopUp(){
        config["deleteScanRecordPopUp"].closePopUp();
    }


    async function handleQuantityModifierQtyClick(event, id){
        if(id !== ""){
            config["updateQuantityPopUp"].showPopUp();
            let quantity = await __getScanQuantity(id);
            config["inputUpdateBundleTicketId"].setValue(id);
            config["inputUpdateQuantity"].setValue(quantity);
            document.getElementById("inputUpdateQuantity").focus();
        }
    }

    async function handleUpdateQuantity(){
        const user = getUser();
        try {
            const bundleTicketId = config["inputUpdateBundleTicketId"].data.value;
            const quantity = config["inputUpdateQuantity"].data.value;
            const dailyScanningSlotId = config["inputScanId"].data.value;
            const dailyShiftTeamId = config["inputDailyShiftTeamId"].data.value;

            if(bundleTicketId !== "" && quantity !== "" && dailyScanningSlotId !== ""){
                config["updateQuantityPopUp"].closePopUp();
                
                const apiRequest = {
                    "bundle_ticket_id": bundleTicketId,
                    "scan_quantity": quantity,
                    "user_id" : user.name
                };

                console.log(JSON.stringify(apiRequest))
    
                document.getElementById("spinner").style.display = "";
    
                const updateQuantity = await API.post(`bundleTickets/updateScannedQuantity`, apiRequest);

                console.log(updateQuantity)
    
                document.getElementById("spinner").style.display = "none";
    
                if (updateQuantity.data.status === "success") {
                    config["CONTROL_CENTER"].promptBaseMessage("Scan Quantity Updated", "");
                    await __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                } else {
                    config["CONTROL_CENTER"].promptErrorMessage("Error", "");
                    
                }

            }else{
                config["CONTROL_CENTER"].promptErrorMessage("Error", "");
            }

        } catch (error) {
            document.getElementById("spinner").style.display = "none";
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    function handleBackButton(){
        history.push(`/setScanningSlot`);
        window.location.reload();
    }

    async function handleScanBundleTicket(packing_list_id_x, original_quantity_x, scan_quantity_x, bundle_id_x) {
        try {
            const bundleTicketId = config["inputBundleId"].data.value;
            const dailyScanningSlotId = config["inputScanId"].data.value;
            const dailyShiftTeamId = config["inputDailyShiftTeamId"].data.value;
            var packing_list_id = null;
            if(packing_list_id_x === 0){
                packing_list_id = null;
            }
            else{
                packing_list_id = packing_list_id_x;
            }
             //packing_list_id_x; //config["inputScanPackingList"].data.value;
            const original_quantity = original_quantity_x; //bundleTicketData[0].BundleTicket[0].original_quantity;
            const scan_quantity = scan_quantity_x; //bundleTicketData[0].BundleTicket[0].original_quantity;
            const bundle_id =bundle_id_x; // bundleTicketData[0].BundleTicket[0].bundle_id;
            const date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            let cDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;


            if(bundleTicketId !== "" && dailyScanningSlotId !== ""){
                console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
                const user = getUser();
                console.log(user , " U S E R ");
                const apiRequest = {
                    "bundle_ticket_id": bundleTicketId,
                    "daily_scanning_slot_id": dailyScanningSlotId,
                    "daily_shift_team_id" : dailyShiftTeamId,
                    "packing_list_id":packing_list_id,
                    "scan_date_time":cDate,
                    "user_id": user.name
                };

                console.log(JSON.stringify(apiRequest));

                document.getElementById("spinner").style.display = "";

                const scanBundleTicket = await API.post(`bundleTickets/scanByScanningSlot`, apiRequest);

                document.getElementById("spinner").style.display = "none";

                if (scanBundleTicket.data.status === "success") {
                    config["CONTROL_CENTER"].promptBaseMessage("Records Updated Successfully", "");
                    await __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                } else {
                    config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                }

                config["inputBundleId"].setValue("");

            }else{
                if(bundleTicketId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Enter Barcode", "");
                }else if(dailyScanningSlotId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Select Scanning Slot", "");
                }else if(packing_list_id ===""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Packing List", "");
                }
            }

            
        } catch (error) {
            document.getElementById("spinner").style.display = "none";
            config["inputBundleId"].setValue("");
            document.getElementById("inputBundleId").focus();
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }


    async function getPackingListStatus(packListId){
        const key = "PackingList";
        const distinct = false;
        const select = ["status"];
        const where = [
            {
                "field-name": "id",
                "operator": "=",
                "value": packListId
            }
        ];
        const relations = [];
        const orderby = "id:desc";
        const limit = 1000;

        const packStatus = await __getDetails(key, distinct, select, where, relations, orderby, limit);
        console.log(packStatus[0].PackingList[0].status , "STATUS STATUS STATUS");

        if(packStatus[0].PackingList[0].status == "Generated"){
            return true;
        }
        else{
            return false;
        }
    }

    async function handleNewInputButtonPress(){
        try{
            document.getElementById("spinner").style.display = "";
            const bundleTicketId = config["inputBundleId"].data.value;
            const dailyScanningSlotId = config["inputScanId"].data.value;
            const dailyShiftTeamId = config["inputDailyShiftTeamId"].data.value;
            var packing_list_id = null; //config["inputScanPackingList"].data.value;
            if(config["inputScanPackingList"].data.value === 0){
                packing_list_id = null;
            }
            else{
                packing_list_id = config["inputScanPackingList"].data.value;
            }
            const original_quantity = config["viewOriginalQty"].data.value;
            const scan_quantity = config["inputScannedQty"].data.value;

            if(original_quantity >= scan_quantity){

                var bundle_id = bundle_id1;
                const date_ob = new Date();
                let date = ("0" + date_ob.getDate()).slice(-2);
                let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
                let year = date_ob.getFullYear();
                let hours = ("0" + (date_ob.getHours() + 1)).slice(-2);
                let minutes = ("0" + (date_ob.getMinutes() + 1)).slice(-2);
                let seconds = ("0" + (date_ob.getSeconds() + 1)).slice(-2);
                let cDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
                var readyToWrite = false;
                var consideringPackingListSoc;
                // console.log(operation_nb, direction_nb, soc_id_nb,  consideringPackingListSoc.qty_json[bundle_size_nb], bundle_size_nb);
                if(operation_nb === "PK" && direction_nb === "IN"){
                    for(var i = 0; i < neededPackingListSOCData.length; i++){
                    if(Number(neededPackingListSOCData[i].packing_list_id) === Number(config["inputScanPackingList"].data.value)){
                            console.log("Matched matched matched");
                            consideringPackingListSoc = neededPackingListSOCData[i];
                        }
                    }

                    console.log(consideringPackingListSoc , "consideringPackingListSoc");
                    var x = await packingListQuantityValidator(soc_id_nb, operation_nb, Number(config["inputScannedQty"].data.value), consideringPackingListSoc.quantity_json[bundle_size_nb], Number(config["inputScanPackingList"].data.value), direction_nb, bundle_size_nb);
                    var y = await getPackingListStatus(Number(config["inputScanPackingList"].data.value));
                    if(x && y){
                        readyToWrite = true;
                    }
                }

                else{
                    readyToWrite = true;
                }
                console.log(readyToWrite, "AAAAAAAAAAAAAA");


                if(readyToWrite){
                    if(bundleTicketId !== "" && dailyScanningSlotId !== "" && dailyShiftTeamId !=="" && original_quantity !== "" && scan_quantity !=="" && bundle_id !==""){
                        const user = getUser();
                        const apiRequest = {
                            "bundle_ticket_id": bundleTicketId,
                            "user_id":user.name,
                            "details" : [{
                            "bundle_ticket_id": bundleTicketId,
                            "daily_scanning_slot_id": dailyScanningSlotId,
                            "daily_shift_team_id" : dailyShiftTeamId,
                            "packing_list_id":packing_list_id,
                            "bundle_id" : bundle_id,
                            "original_quantity" : original_quantity,
                            "scan_quantity" : scan_quantity,
                            "scan_date_time" : cDate,
                            "user_id":user.name
                            }]
                        }

                        console.log(JSON.stringify(apiRequest));
                        var scanBundleTicket1;
                        try{
                            scanBundleTicket1 = await API.post(`bundleTicketsSecondary/createNewRecordChecked`, apiRequest);
                            console.log(scanBundleTicket1);
                        }
                        catch(error){
                            
                        }
                        document.getElementById("spinner").style.display = "none";
                        if (scanBundleTicket1.status === 200) {
                            window.location.reload();
                            //config["CONTROL_CENTER"].promptBaseMessage("Records Updated Successfully", "");
                            await __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                        } else if (scanBundleTicket1.status === "error"){
                            //config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                            document.getElementById("spinner").style.display = "none";
                            let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
                            config["inputSecondPackingList"].setValue("");
                            config["inputSecondPackingList"].setOptions(dropdownOptionsz);
                            config["viewOriginalQty"].setValue("");
                            config["viewScanedQty"].setValue("");
                            config["inputScannedQty"].setValue("");
                            config["inputBundleId"].setValue("");
                            config["newCheckBox"].setDisabled(false);
                            if(document.getElementById("newCheckBox").checked){
                                document.getElementById("newCheckBox").click();
                                document.getElementById("newCheckBox").checked = false;
                            }
                        }
                        else{
                            document.getElementById("spinner").style.display = "none";
                            let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
                            config["inputSecondPackingList"].setValue("");
                            config["inputSecondPackingList"].setOptions(dropdownOptionsz);
                            config["viewOriginalQty"].setValue("");
                            config["viewScanedQty"].setValue("");
                            config["inputScannedQty"].setValue("");
                            config["inputBundleId"].setValue("");
                            config["newCheckBox"].setDisabled(false);
                            if(document.getElementById("newCheckBox").checked){
                                document.getElementById("newCheckBox").click();
                                document.getElementById("newCheckBox").checked = false;
                            }
                        }

                        config["inputBundleId"].setValue("");

                    }else{
                        document.getElementById("spinner").style.display = "none";

                        if(bundleTicketId === ""){
                            config["CONTROL_CENTER"].promptWarningMessage("Please Enter Barcode", "");
                        }else if(dailyScanningSlotId === ""){
                            config["CONTROL_CENTER"].promptWarningMessage("Please Select Scanning Slot", "");
                        }else if(packing_list_id ===""){
                            config["CONTROL_CENTER"].promptWarningMessage("Please Scan Packing List", "");
                        }
                    }
                }
                else{
                    document.getElementById("spinner").style.display = "none";
                    config["CONTROL_CENTER"].promptWarningMessage("Error", "Quantity exceeds for the packing list.");
                }
        }
        else{
            document.getElementById("spinner").style.display = "none";
            config["CONTROL_CENTER"].promptWarningMessage("New scanned quantity cannot be greater than the original quantity", "");
        }

        }
        catch (error){
            document.getElementById("spinner").style.display = "none";
            let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
            config["inputSecondPackingList"].setValue("");
            config["inputSecondPackingList"].setOptions(dropdownOptionsz);
            config["viewOriginalQty"].setValue("");
            config["viewScanedQty"].setValue("");
            config["inputScannedQty"].setValue("");
            config["inputBundleId"].setValue("");
            config["newCheckBox"].setDisabled(false);
            document.getElementById("inputBundleId").focus();
            if(document.getElementById("newCheckBox").checked){
                document.getElementById("newCheckBox").click();
                document.getElementById("newCheckBox").checked = false;
            }
            try {
                if (error.response.data.message) {
                    document.getElementById("spinner").style.display = "none";
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptErrorMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }


    async function socValidator(){
        try{
            // config["newInputButton"].setDisabled(flase);
            document.getElementById("spinner").style.display = "";
            console.log("Came inside");
            //get fpo_operation_id from bundle ticket
            const bundleTicketId = config["inputBundleId"].data.value;
            const key = "BundleTicket";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": bundleTicketId
                }
            ];
            const relations = ["bundle", "fpo_operation"];
            const orderby = "id:desc";
            const limit = 1000;

            const bundleTicketData = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            neededBundleTicketData = bundleTicketData;
            console.log(bundleTicketData[0].BundleTicket[0] , "FPO OPERATION Id");

            const bundleID = bundleTicketData[0].BundleTicket[0].bundle_id;
            bundle_id1 = bundleID;
            const bundleData = bundleTicketData[0].BundleTicket[0].bundle;
            const bundleSize = bundleData.size;
            bundle_size_nb = bundleSize;
            const fpoOperationData = bundleTicketData[0].BundleTicket[0].fpo_operation;
            console.log(bundleData.size , "BUNDLE SIZE");


            //getPackingListId from job cards table

            const key5 = "JobCardBundle";
            const distinct5 = false;
            const select5 = ["*"];
            const where5 = [
                {
                    "field-name": "bundle_id",
                    "operator": "=",
                    "value": bundleID
                }
            ];
            const relations5 = ["job_card"];
            const orderby5 = "id:desc";
            const limit5 = 1000;

            const jobCardBundleData = await __getDetails(key5, distinct5, select5, where5, relations5, orderby5, limit5);
            const jobCardId = jobCardBundleData[0].JobCardBundle[0].job_card_id;
            const jobCardData = jobCardBundleData[0].JobCardBundle[0].job_card;
            console.log(jobCardBundleData[0].JobCardBundle[0] , " job_card_id");

            var jobCardPackingListId = 0;
            if(jobCardData.packing_list_no){
                jobCardPackingListId = jobCardData.packing_list_no;
                console.log(jobCardData.packing_list_no , " job_card_packing_list_id");
            }

            // get soc id from fpo table
            
            const fpoId = fpoOperationData.fpo_id;
            const key2 = "Fpo";
            const distinct2 = false;
            const select2 = ["*"];
            const where2 = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": fpoId
                }
            ];
            const relations2 = [];
            const orderby2 = "id:desc";
            const limit2 = 1000;

            const fpoData = await __getDetails(key2, distinct2, select2, where2, relations2, orderby2, limit2);
            console.log(fpoData[0].Fpo[0].soc_id     , "SOC ID");
            soc_id_nb = fpoData[0].Fpo[0].soc_id;


            // get soc_id from packing_list_soc
            const ss = fpoData[0].Fpo[0].soc_id;
            const key3 = "PackingListSoc";
            const distinct3 = false;
            const select3 = ["*"];
            const where3 = [
                {
                    "field-name": "soc_id",
                    "operator": "=",
                    "value": ss
                }
            ];
            const relations3 = [];
            const orderby3 = "id:desc";
            const limit3 = 1000;

            const packingListSoc = await __getDetails(key3, distinct3, select3, where3, relations3, orderby3, limit3);
            neededPackingListSOCData = packingListSoc[0].PackingListSoc;
            console.log(packingListSoc[0].PackingListSoc , "PACKING LIST SOC");
            var qty_json_value = 0;
            console.log(fpoOperationData.operation , "fpoOperationData.operation");
            console.log(bundleTicketData[0].BundleTicket[0].direction , "bundleTicketData[0].BundleTicket[0].direction");
            operation_nb = fpoOperationData.operation;
            direction_nb = bundleTicketData[0].BundleTicket[0].direction;
            if(packingListSoc[0].PackingListSoc.length === 0){
                if(operation_nb === "PK" && direction_nb === "IN"){
                    document.getElementById("spinner").style.display = "none";
                    config["CONTROL_CENTER"].promptWarningMessage("Error", "No paking list SOCs.");
                }
                else{
                    if(!showWhenChecked1){
                        document.getElementById("spinner").style.display = "none";
                        config["inputScanPackingList"].setValue(0);
                        await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                    }
                    else{
                        document.getElementById("spinner").style.display = "none";
                        config["inputScanPackingList"].setValue(0);
                        let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                        config["inputSecondPackingList"].setOptions(dropdownOptions);
                        config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                        config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                        config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                        bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                        if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                            config["inputScannedQty"].setValue(0);
                            config["newInputButton"].setDisabled(true);
                        }
                        else{
                            config["newInputButton"].setDisabled(false);
                        }
                    }
                }


            }

            if(packingListSoc[0].PackingListSoc.length === 1){
                config["inputScanPackingList"].setValue(packingListSoc[0].PackingListSoc[0].packing_list_id);
                let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
                config["inputSecondPackingList"].setValue("");
                config["inputSecondPackingList"].setOptions(dropdownOptionsz);
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["newCheckBox"].setDisabled(false);
                // if(document.getElementById("newCheckBox").checked){
                //     document.getElementById("newCheckBox").click();
                //     document.getElementById("newCheckBox").checked = false;
                // }
                qty_json_value = packingListSoc[0].PackingListSoc[0].quantity_json[bundleSize];
                if(operation_nb === "PK" && direction_nb === "IN"){

                    if(jobCardPackingListId === 0){
                        //alert("You need to have a packing list in your job card");
                        if(await getPackingListStatus(packingListSoc[0].PackingListSoc[0].packing_list_id)){
                            config["inputScanPackingList"].setValue(packingListSoc[0].PackingListSoc[0].packing_list_id);
                        }
                        else{
                            alert("You need to have a packing list in your job card and the status should be Generated.");
                            document.getElementById("spinner").style.display = "none";
                            return false;
                        }
                    }
                    else if(jobCardPackingListId !== 0){
                        var matching = false;
                        for(var r = 0; r<packingListSoc[0].PackingListSoc.length; r++){
                            if(jobCardPackingListId === packingListSoc[0].PackingListSoc[r].packing_list_id){
                                matching = true;
                            }
                        }
                        if(matching){
                            if(await getPackingListStatus(jobCardData.packing_list_no)){
                                config["inputScanPackingList"].setValue(jobCardData.packing_list_no);
                                console.log("JOB CARD PACKING LIST SET");
                            }
                            else{
                                alert("Packing list status should be generated.");
                                document.getElementById("spinner").style.display = "none";
                                return false;                            }
                        }
                        else{
                            if(await getPackingListStatus(packingListSoc[0].PackingListSoc[0].packing_list_id)){
                                config["inputScanPackingList"].setValue(packingListSoc[0].PackingListSoc[0].packing_list_id); 
                            }
                            else{
                                alert("Packing list status should be generated.");
                                document.getElementById("spinner").style.display = "none";
                                return false;                            }
                        }
                    }
                    else{
                    }

                    var quantityValidation = await packingListQuantityValidator(packingListSoc[0].PackingListSoc[0].soc_id, operation_nb, bundleTicketData[0].BundleTicket[0].original_quantity, qty_json_value, config["inputScanPackingList"].data.value, direction_nb, bundleSize);

                    if(quantityValidation){
                        if(!showWhenChecked1){
                            await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                        }
                        else{
                            
                            document.getElementById("spinner").style.display = "none";
                            let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                            for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                dropdownOptions.push({
                                    "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                });
                            }
                            config["inputSecondPackingList"].setOptions(dropdownOptions);

                            config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                            config["inputSecondPackingList"].setValue(jobCardData.packing_list_no);
                            config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                            config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                            bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                            if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                config["inputScannedQty"].setValue(0);
                                config["newInputButton"].setDisabled(true);
                            }
                            else{
                                config["newInputButton"].setDisabled(false);
                            }
                        }   
                    }
                    else{
                        document.getElementById("spinner").style.display = "none";
                        config["CONTROL_CENTER"].promptWarningMessage("Error", "Quantity exceeds for the size.");
                    }
                }
                else{
                    var jobCardPackingListIdSelected = 0;
                    if(jobCardPackingListId === 0){
                        if(!showWhenChecked1){
                            //await getPackingListStatus(packingListSoc[0].PackingListSoc[0].packing_list_id);
                            config["inputScanPackingList"].setValue(packingListSoc[0].PackingListSoc[0].packing_list_id);
                            await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                        }
                        else{
                            document.getElementById("spinner").style.display = "none";
                            if(!document.getElementById("newCheckBox").checked){
                                document.getElementById("newCheckBox").click();
                            }
                            let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                            for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                dropdownOptions.push({
                                    "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                });
                            }
                            config["inputSecondPackingList"].setOptions(dropdownOptions);
                            config["inputSecondPackingList"].setValue(jobCardPackingListIdSelected);
                            config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                            config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                            config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                            bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                            if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                config["inputScannedQty"].setValue(0);
                                config["newInputButton"].setDisabled(true);
                            }
                            else{
                                config["newInputButton"].setDisabled(false);
                            }
                            config["newCheckBox"].setDisabled(true);
                        }
                    }
                    else if(jobCardPackingListId !== 0){
                        var matching = false;
                        for(var r = 0; r<packingListSoc[0].PackingListSoc.length; r++){
                            if(Number(jobCardPackingListId) === Number(packingListSoc[0].PackingListSoc[r].packing_list_id)){
                                matching = true;
                                //jobCardPackingList = packingListSoc[0].PackingListSoc[r];
                                jobCardPackingListIdSelected = Number(packingListSoc[0].PackingListSoc[r].packing_list_id);
                            }
                        }
                        if(matching){
                            //await getPackingListStatus(packingListSoc[0].PackingListSoc[0].packing_list_id);
                            config["inputScanPackingList"].setValue(jobCardData.packing_list_no);
                            if(!showWhenChecked1){
                                await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                            }
                            else{
                                if(!document.getElementById("newCheckBox").checked){
                                    document.getElementById("newCheckBox").click();
                                }
                                let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                                for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                    dropdownOptions.push({
                                        "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                    });
                                }
                                config["inputSecondPackingList"].setOptions(dropdownOptions);
                                config["inputSecondPackingList"].setValue(jobCardPackingListIdSelected);
                                config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                                config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                                config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                                bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                                if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                    config["inputScannedQty"].setValue(0);
                                    config["newInputButton"].setDisabled(true);
                                }
                                else{
                                    config["newInputButton"].setDisabled(false);
                                }
                                config["newCheckBox"].setDisabled(true);
                            }

                        }
                        else{
                            await handleScanBundleTicket(0, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                        }
                    }
                    else{
                    }
                }
            }

            if(packingListSoc[0].PackingListSoc.length > 1){
                let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
                config["inputSecondPackingList"].setValue("");
                config["inputSecondPackingList"].setOptions(dropdownOptionsz);
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["newCheckBox"].setDisabled(false);
                if(direction_nb === "IN" && operation_nb === "PK"){
                    var jobCardPackingList;
                    var jobCardPackingListIdSelected = 0;
                    console.log(jobCardPackingListId);
                    if(jobCardPackingListId === 0){
                        if(!document.getElementById("newCheckBox").checked){
                            document.getElementById("newCheckBox").click();
                        }
                        let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                        for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                            dropdownOptions.push({
                                "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                            });
                        }
                        config["inputSecondPackingList"].setOptions(dropdownOptions);
                        config["viewOriginalQty"].setValue(Math.floor(bundleTicketData[0].BundleTicket[0].original_quantity ));
                        config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                        config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                        bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                        if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                            config["inputScannedQty"].setValue(0);
                            config["newInputButton"].setDisabled(true);
                        }
                        else{
                            config["newInputButton"].setDisabled(false);
                        }
                        config["newCheckBox"].setDisabled(true);
                    }
                    else if(jobCardPackingListId !== 0){
                        var matching = false;
                        for(var r = 0; r<packingListSoc[0].PackingListSoc.length; r++){
                            if(Number(jobCardPackingListId) === Number(packingListSoc[0].PackingListSoc[r].packing_list_id)){
                                matching = true;
                                jobCardPackingList = packingListSoc[0].PackingListSoc[r];
                                jobCardPackingListIdSelected = Number(packingListSoc[0].PackingListSoc[r].packing_list_id);
                            }
                        }
                        if(matching){
                            qty_json_value = jobCardPackingList.quantity_json[bundleSize];
                            var quantityOk = await packingListQuantityValidator(jobCardPackingList.soc_id, operation_nb, bundleTicketData[0].BundleTicket[0].original_quantity, qty_json_value, jobCardPackingListIdSelected, direction_nb, bundleSize);

                            if(quantityOk){
                                if(!showWhenChecked1){
                                    if(await getPackingListStatus(jobCardPackingListIdSelected)){
                                        config["inputScanPackingList"].setValue(jobCardPackingListIdSelected);
                                        await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                                    }
                                    else{
                                        alert("Packing list status should be Generated.");
                                        document.getElementById("spinner").style.display = "none";
                                        return false;                                    }
                                }
                                else{
                                    if(!document.getElementById("newCheckBox").checked){
                                        document.getElementById("newCheckBox").click();
                                    }
                                    let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                                    for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                        dropdownOptions.push({
                                            "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                        });
                                    }
                                    config["inputSecondPackingList"].setOptions(dropdownOptions);
                                    config["viewOriginalQty"].setValue(Math.floor(bundleTicketData[0].BundleTicket[0].original_quantity ));
                                    config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                                    config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                                    bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                                    if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                        config["inputScannedQty"].setValue(0);
                                        config["newInputButton"].setDisabled(true);
                                    }
                                    else{
                                        config["newInputButton"].setDisabled(false);
                                    }
                                    config["newCheckBox"].setDisabled(true);
                                    config["inputScanPackingList"].setValue(jobCardData.packing_list_no);
                                    config["inputSecondPackingList"].setValue(jobCardData.packing_list_no);
                                }
                            }
                            else{
                                if(!document.getElementById("newCheckBox").checked){
                                    document.getElementById("newCheckBox").click();
                                }
                                let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                                for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                    dropdownOptions.push({
                                        "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                    });
                                }
                                config["inputSecondPackingList"].setOptions(dropdownOptions);
                                config["viewOriginalQty"].setValue(Math.floor(bundleTicketData[0].BundleTicket[0].original_quantity ));
                                config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                                config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                                bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                                if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                    config["inputScannedQty"].setValue(0);
                                    config["newInputButton"].setDisabled(true);
                                }
                                else{
                                    config["newInputButton"].setDisabled(false);
                                }
                                config["newCheckBox"].setDisabled(true);
                            }
                        }
                        else { //not matching
                            if(!document.getElementById("newCheckBox").checked){
                                document.getElementById("newCheckBox").click();
                            }
                            let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                            for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                dropdownOptions.push({
                                    "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                });
                            }
                            config["inputSecondPackingList"].setOptions(dropdownOptions);
                            config["viewOriginalQty"].setValue(Math.floor(bundleTicketData[0].BundleTicket[0].original_quantity ));
                            config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                            config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                            bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                            if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                config["inputScannedQty"].setValue(0);
                                config["newInputButton"].setDisabled(true);
                            }
                            else{
                                config["newInputButton"].setDisabled(false);
                            }
                            config["newCheckBox"].setDisabled(true);
                        }
                    }
                    else{
                    }
                    

                    console.log("PACKING IN PACKING IN PACKING IN");
                    
                }
                else{
                    var jobCardPackingListIdSelected = 0;
                    if(jobCardPackingListId === 0){
                        await handleScanBundleTicket(0, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                    }
                    else if(jobCardPackingListId !== 0){
                        var matching = false;
                        for(var r = 0; r<packingListSoc[0].PackingListSoc.length; r++){
                            if(Number(jobCardPackingListId) === Number(packingListSoc[0].PackingListSoc[r].packing_list_id)){
                                matching = true;
                                //jobCardPackingList = packingListSoc[0].PackingListSoc[r];
                                jobCardPackingListIdSelected = Number(packingListSoc[0].PackingListSoc[r].packing_list_id);
                            }
                        }
                        if(matching){
                            await getPackingListStatus(packingListSoc[0].PackingListSoc[0].packing_list_id);
                            config["inputScanPackingList"].setValue(jobCardData.packing_list_no);
                            if(!showWhenChecked1){
                                await handleScanBundleTicket(config["inputScanPackingList"].data.value, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                            }
                            else{
                                if(!document.getElementById("newCheckBox").checked){
                                    document.getElementById("newCheckBox").click();
                                }
                                let dropdownOptions = [{ value: "", text: "- Select Packing List -" }];
                                for(var x = 0; x < packingListSoc[0].PackingListSoc.length; x++){
                                    dropdownOptions.push({
                                        "value" : packingListSoc[0].PackingListSoc[x].packing_list_id , "text" :packingListSoc[0].PackingListSoc[x].packing_list_id
                                    });
                                }
                                config["inputSecondPackingList"].setOptions(dropdownOptions);
                                config["inputSecondPackingList"].setValue(jobCardPackingListIdSelected);
                                config["viewOriginalQty"].setValue(Math.floor(bundleTicketData[0].BundleTicket[0].original_quantity ));
                                config["viewScanedQty"].setValue(bundleTicketData[0].BundleTicket[0].scan_quantity);
                                config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - bundleTicketData[0].BundleTicket[0].scan_quantity);
                                bundle_id1 = bundleTicketData[0].BundleTicket[0].bundle_id;
                                if(bundleTicketData[0].BundleTicket[0].original_quantity <= bundleTicketData[0].BundleTicket[0].scan_quantity){
                                    config["inputScannedQty"].setValue(0);
                                    config["newInputButton"].setDisabled(true);
                                }
                                else{
                                    config["newInputButton"].setDisabled(false);
                                }
                                config["newCheckBox"].setDisabled(true);
                            }

                        }
                        else{
                            await handleScanBundleTicket(0, bundleTicketData[0].BundleTicket[0].original_quantity, bundleTicketData[0].BundleTicket[0].original_quantity, bundleID);
                        }
                    }
                    else{
                    }
                }
                document.getElementById("spinner").style.display = "none";
            }
        }
        catch (error){
            document.getElementById("spinner").style.display = "none";
            config["inputBundleId"].setValue("");
            document.getElementById("inputBundleId").focus();
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
            return false;
        }
    }


    async function packingListQuantityValidator(socId , fpoOperationX, scanQuantity , maxQuantity, packingListId, direction, bundleSize){
        try{
            if(fpoOperationX !== "PK" && direction !== "IN"){
                return true;
            }
            else{
            console.log(scanQuantity , "Scan Quantity 2nd");
            console.log(maxQuantity , "Max Quantity 2nd");
            //find fpos for given soc
            const socID = socId;
            const key = "Fpo";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "soc_id",
                    "operator": "=",
                    "value": socID
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1000;

            const FpoData = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            console.log(FpoData, "FPO 2nd validation");

            var totalScanCount = 0;

            for(var i = 0; i < FpoData[0].Fpo.length; i++){
                const FpoId = FpoData[0].Fpo[i].id;
                const fpoOperationID = fpoOperationX;
                const key1 = "FpoOperation";
                const distinct1 = false;
                const select1 = ["*"];
                const where1 = [
                    {
                        "field-name": "fpo_id",
                        "operator": "=",
                        "value": FpoId
                    },
                    {
                        "field-name": "operation",
                        "operator": "=",
                        "value": fpoOperationID
                    }
                ];
                const relations1 = [];
                const orderby1 = "id:desc";
                const limit1 = 1000;

                const FpoOperationData = await __getDetails(key1, distinct1, select1, where1, relations1, orderby1, limit1);
                console.log(FpoOperationData[0].FpoOperation , "FPO OPERATION 2nd validation");

                for(var j = 0; j < FpoOperationData[0].FpoOperation.length; j++){
                    console.log(bundleSize, "BUNDLE SIZE 2nd");
                    const fpoOperationIDD = FpoOperationData[0].FpoOperation[j].id;
                    const key2 = "BundleTicket";
                    const distinct2 = false;
                    const select2 = ["*"];
                    const where2 = [
                        {
                            "field-name": "fpo_operation_id",
                            "operator": "=",
                            "value": fpoOperationIDD
                        },
                        {
                            "field-name": "packing_list_id",
                            "operator": "=",
                            "value": packingListId
                        },
                        {
                            "field-name": "direction",
                            "operator": "=",
                            "value": direction
                        }
                    ];
                    const relations2 = ["bundle"];
                    const orderby2 = "id:desc";
                    const limit2 = 1000;

                    const BundleTicketData = await __getDetails(key2, distinct2, select2, where2, relations2, orderby2, limit2);
                    console.log(BundleTicketData[0].BundleTicket , "Bundle Ticket 2nd validation");

                    for(var k = 0; k < BundleTicketData[0].BundleTicket.length; k++){
                        if(BundleTicketData[0].BundleTicket[k].scan_quantity && BundleTicketData[0].BundleTicket[k].bundle.size === bundleSize){
                            totalScanCount = totalScanCount + BundleTicketData[0].BundleTicket[k].scan_quantity;
                        }
                    }
                }
            }
            console.log(totalScanCount + Number(scanQuantity) , "Total Scan Quantity");
            if(totalScanCount + Number(scanQuantity) <= maxQuantity){
                return true;
            }
            else
            {
                document.getElementById("spinner").style.display = "none";
                // config["CONTROL_CENTER"].promptErrorMessage("Error", "SOC quantity exceeds");
                // let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
                // config["inputSecondPackingList"].setValue("");
                // config["inputSecondPackingList"].setOptions(dropdownOptionsz);
                // config["viewOriginalQty"].setValue("");
                // config["viewScanedQty"].setValue("");
                // config["inputScannedQty"].setValue("");
                // config["inputBundleId"].setValue("");
                // config["newCheckBox"].setDisabled(false);
                // document.getElementById("inputBundleId").focus();
                // if(document.getElementById("newCheckBox").checked){
                //     document.getElementById("newCheckBox").click();
                //     document.getElementById("newCheckBox").checked = false;
                // }
                return false;
            }
        }

        }
        catch(error){
            document.getElementById("spinner").style.display = "none";
            config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            let dropdownOptionsz = [{ value: "", text: "- Select Packing List -" }];
            config["inputSecondPackingList"].setValue("");
            config["inputSecondPackingList"].setOptions(dropdownOptionsz);
            config["viewOriginalQty"].setValue("");
            config["viewScanedQty"].setValue("");
            config["inputScannedQty"].setValue("");
            config["inputBundleId"].setValue("");
            config["newCheckBox"].setDisabled(false);
            document.getElementById("inputBundleId").focus();
            if(document.getElementById("newCheckBox").checked){
                document.getElementById("newCheckBox").click();
                document.getElementById("newCheckBox").checked = false;
            }
            return false;
        }
    }

    function onChange(event) {
        event.preventDefault();
        alert("This is the place where you write CHANGE")
    }

    function onPopulate(event) {
        event.preventDefault();
        alert("This is the place where you write POPULATE")
    }

    function onNew() {
        let dataArray = {};
        //Action handling when NEW buttion clicked...
        alert("This is the place where you write NEW")
        return dataArray
    }

    function onDelete() {
        //Action handling when DELETE buttion clicked...
        alert("This is the place where you write DELETE")
    }

    function onRefresh() {
        //Action handling when REFRESH buttion clicked...
        alert("This is the place where you write REFRESH")
    }

    function onSaveNew(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveModify(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveDelete(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    return generateScanBundleDisplay(config)
}
    
export default ScanBundle;