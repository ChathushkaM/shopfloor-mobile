import { event } from "jquery"

let componentListConfig = []

componentListConfig["CONTROL_CENTER"] = {
    objectType: "Controller",
    schema: {
        id: "formScanBundle",
        name: "formScanBundle",
        controllerObject: componentListConfig,
        create: true,
        createAPI: "",
        read: true,
        readAPI: "",
        update: true,
        updateAPI: "",
        delete: false,
        deleteAPI: ""
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanBundle",
            type: "text",
            visible: true,
            value: "Scan Bundle"
        },
    },
    state: {
        populated: false,
        modified: false,
        deleted: false,
        new: false
    },
    actions: {
        save: "buttonSave",
        delete: "buttonDelete",
        populate: "buttonPopulate",
        refresh: "buttonRefresh"
    },
    event: {
    }
}

componentListConfig["buttonPopulate"] = {
    objectType: "Button",
    schema: {
        id: "buttonPopulate",
        name: "buttonPopulate",
        type: "submit",
        label: "Populate",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonNew"] = {
    objectType: "Button",
    schema: {
        id: "buttonNew",
        name: "buttonNew",
        type: "submit",
        label: "New",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonSave"] = {
    objectType: "Button",
    schema: {
        id: "buttonSave",
        name: "buttonSave",
        type: "submit",
        label: "Save",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonRefresh"] = {
    objectType: "Button",
    schema: {
        id: "buttonRefresh",
        name: "buttonRefresh",
        type: "submit",
        label: "Clear",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDelete"] = {
    objectType: "Button",
    schema: {
        id: "buttonDelete",
        name: "buttonDelete",
        type: "submit",
        label: "Delete",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputScanDate"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanDate",
        placeholder: "Date",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanDate",
            type: "text",
            visible: true,
            value: "Date"
        },
        class: ""
    },
    data: {
        sqlcolumn: "scan_date",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputScanShift"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanShift",
        placeholder: "Shift",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanShift",
            type: "text",
            visible: true,
            value: "Shift"
        },
        class: ""
    },
    data: {
        sqlcolumn: "scan_shift",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputScanTeam"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanTeam",
        placeholder: "Team",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanTeam",
            type: "text",
            visible: true,
            value: "Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "scan_team",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputScanSlot"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanSlot",
        placeholder: "Slot",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanSlot",
            type: "text",
            visible: true,
            value: "Slot"
        },
        class: ""
    },
    data: {
        sqlcolumn: "scan_slot",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

// new check box
componentListConfig["newCheckBox"] = {
    objectType: "CheckBox",
    schema: {
        id : "newCheckBox",
        name: "newCheckBox",
        placeholder: "",
        type: "checkbox",
        label: "Change Quantity",
        checkedValue: "1",
        uncheckedValue: "0",
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelNewCheckBox",
            type: "text",
            visible: true,
            value: "New  Box"
        },
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}


componentListConfig["inputSecondPackingList"] = {
    objectType: "DropDown",
    schema: {
        id : "inputSecondPackingList",
        name: "inputSecondPackingList",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanPackingList",
            type: "text",
            visible: true,
            value: "Packing List"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    options: [{ value: "", text: "- Select Packing List -" }],
    class: "",
    event: {}
}

componentListConfig["viewOriginalQty"] = {
    objectType: "TextBox",
    schema: {
        name: "viewOriginalQty",
        placeholder: "Original Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelOriginalQty",
            type: "text",
            visible: true,
            value: "original Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: 0
    },
    class: "",
    event: {}
}

componentListConfig["viewScanedQty"] = {
    objectType: "TextBox",
    schema: {
        name: "viewScanedQty",
        placeholder: "Scaned Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanedQty",
            type: "text",
            visible: true,
            value: "Scanned Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputScannedQty"] = {
    objectType: "TextBox",
    schema: {
        id: "inputScannedQty",
        name: "inputScannedQty",
        placeholder: "Enter Scanned Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelOriginalQty",
            type: "text",
            visible: true,
            value: "New Scanned Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}


componentListConfig["inputScanPackingList"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanPackingList",
        placeholder: "PackingList",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanPackingList",
            type: "text",
            visible: true,
            value: "Packing List"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputScanId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputScanId",
        placeholder: "Scan Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanId",
            type: "text",
            visible: false,
            value: "Scan Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "scan_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputDailyShiftTeamId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputDailyShiftTeamId",
        placeholder: "Daily Shift Team",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelDailyShiftTeamId",
            type: "text",
            visible: false,
            value: "Daily Shift Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "daily_shift_team_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBundleId"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputBundleId",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBundleId",
            type: "text",
            visible: true,
            value: "Bundle Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "bundle_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["quantityModifier"] = {
    objectType: "QuantityModifier",
    schema:{
        name: "quantityModifier",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
        quantityList: [],
    },
    event:{}
}

componentListConfig["deleteMasterPopUp"] = {
    objectType: "PopUpPage",
    schema:{
        name: "deleteMasterPopUp",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
    },
    event:{}
}

componentListConfig["buttonDeleteMasterYes"] = {
    objectType: "Button",
    schema: {
        id: "buttonDeleteMasterYes",
        name: "buttonDeleteMasterYes",
        type: "submit",
        label: "Yes",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDeleteMasterNo"] = {
    objectType: "Button",
    schema: {
        id: "buttonDeleteMasterNo",
        name: "buttonDeleteMasterNo",
        type: "submit",
        label: "No",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}


componentListConfig["backButton"] = {
    objectType: "Button",
    schema: {
        id: "backButton",
        name: "backButton",
        type: "submit",
        label: "Back",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["newInputButton"] = {
    objectType: "Button",
    schema: {
        id: "newInputButton",
        name: "newInputButton",
        type: "submit",
        label: "Enter",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["updateQuantityPopUp"] = {
    objectType: "PopUpPage",
    schema:{
        name: "updateQuantityPopUp",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
    },
    event:{}
}

componentListConfig["inputUpdateQuantity"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputUpdateQuantity",
        placeholder: "Quantity",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelUpdateQuantity",
            type: "text",
            visible: true,
            value: "Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "update_quantity",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputUpdateBundleTicketId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputUpdateBundleTicketId",
        placeholder: "Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelUpdateBundleTicketId",
            type: "text",
            visible: false,
            value: "Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "update_bundle_ticket_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["buttonUpdateQuantity"] = {
    objectType: "Button",
    schema: {
        id: "buttonUpdateQuantity",
        name: "buttonUpdateQuantity",
        type: "submit",
        label: "Update",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["deleteScanRecordPopUp"] = {
    objectType: "PopUpPage",
    schema:{
        name: "deleteScanRecordPopUp",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
    },
    event:{}
}

componentListConfig["inputDeleteBundleTicketId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputDeleteBundleTicketId",
        placeholder: "Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelDeleteBundleTicketId",
            type: "text",
            visible: false,
            value: "Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "delete_bundle_ticket_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["buttonYesPopUp"] = {
    objectType: "Button",
    schema: {
        id: "buttonYesPopUp",
        name: "buttonYesPopUp",
        type: "submit",
        label: "Yes",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonNoPopUp"] = {
    objectType: "Button",
    schema: {
        id: "buttonNoPopUp",
        name: "buttonNoPopUp",
        type: "submit",
        label: "No",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

export default componentListConfig