import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField, QuantityModifier } from '../../../BASE/Components'

export function generateScanBundleDisplay(componentList, control) {
    return (
        <>
            <div className="loading" id="spinner" style={{display:"none"}}>Loading&#8230;</div>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>

            <PopUpPage item={componentList["updateQuantityPopUp"]} headerText="Update Quantity" className="">
                <div className="row p-3">
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Label item={componentList["inputUpdateQuantity"].label} />
                                <IntegerField item={componentList["inputUpdateQuantity"]} className="form-control form-control-sm" />
                                <TextBox item={componentList["inputUpdateBundleTicketId"]} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Button className="btn btn-success float-right" item={componentList["buttonUpdateQuantity"]}> Update</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </PopUpPage>

            <PopUpPage item={componentList["deleteScanRecordPopUp"]} headerText="Confirm Delete" className="">
                <div className="row p-4 float-right">
                    <TextBox item={componentList["inputDeleteBundleTicketId"]} />
                    <Button className="btn btn-danger mr-2" item={componentList["buttonYesPopUp"]}> Yes</Button>
                    <Button className="btn btn-info" item={componentList["buttonNoPopUp"]}> No</Button>
                </div>
            </PopUpPage>

            <TextBox item={componentList["inputScanId"]} />
            <TextBox item={componentList["inputDailyShiftTeamId"]} />
            <div className="container-fluid bg-light" >
                <div className="row">
                    <div className="col-6">
                        <div className="form-group row mb-0">
                            <label htmlFor="inputScanDate" className="col-4 col-form-label header-label">Date</label>
                            <div className="col-8 p-0">
                                <TextBox item={componentList["inputScanDate"]} className="form-control-plaintext header-content" />
                            </div>
                        </div>
                        <div className="form-group row mb-0">
                            <label htmlFor="inputScanTeam" className="col-4 col-form-label header-label">Team</label>
                            <div className="col-8 p-0">
                                <TextBox item={componentList["inputScanTeam"]} className="form-control-plaintext header-content" />
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-group row mb-0">
                            <label htmlFor="inputScanShift" className="col-4 col-form-label header-label">Shift</label>
                            <div className="col-8 p-0">
                                <TextBox item={componentList["inputScanShift"]} className="form-control-plaintext header-content" />
                            </div>
                        </div>
                        <div className="form-group row mb-0">
                            <label htmlFor="inputScanSlot" className="col-4 col-form-label header-label">Slot</label>
                            <div className="col-8 p-0">
                                <TextBox item={componentList["inputScanSlot"]} className="form-control-plaintext header-content" />    
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-group row mb-0">
                            <label htmlFor="inputScanPackingList" className="col-4 col-form-label header-label">Packing List</label>
                            <div className="col-8 p-0">
                                <TextBox item={componentList["inputScanPackingList"]} className="form-control-plaintext header-content" />    
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="form-group row mb-0">
                            {/* <label htmlFor="newCheckBox" className="col-4 col-form-label header-label">New Check Box</label> */}
                            <div className="form-check col-8 p-0" style= {{marginLeft:"35px"}}>
                                <CheckBox item={componentList["newCheckBox"]} className="form-check-input" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid">
                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <label htmlFor="inputBundleId" className="col-form-label main-label pt-3">Barcode</label>
                        <IntegerField item={componentList["inputBundleId"]} className="form-control form-control-lg text-center" />
                    </div>
                </div>
            </div>


            <br>
            </br>

            <hr/>

            <div className="container-fluid" id="showWhenChecked" style={{borderStyle:"solid" , borderColor:"blue"}}>
                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <label htmlFor="inputSecondPackingList" className="col-4 col-form-label header-label">Packing List</label>
                        <DropDown item={componentList["inputSecondPackingList"]} className="form-control form-control-lg text-center" />    
                    </div>
                </div>

                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <label htmlFor="viewOriginalQty" className="col-4 col-form-label header-label">Original Quantity</label>
                        <TextBox item={componentList["viewOriginalQty"]} className="form-control form-control-lg text-center" />    
                    </div>
                </div>

                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <label htmlFor="viewScanedQty" className="col-4 col-form-label header-label">Already Scaned Quantity</label>
                        <TextBox item={componentList["viewScanedQty"]} className="form-control form-control-lg text-center" />    
                    </div>
                </div>

                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <label htmlFor="inputScannedQty" className="col-4 col-form-label header-label">New Scaned Quantity</label>
                        <TextBox item={componentList["inputScannedQty"]} className="form-control form-control-lg text-center" />    
                    </div>
                </div>

                <br>
                </br>

                <div className="row pb-1">
                    <div className="col-md-12 text-center">
                        <Button className="btn btn-primary" item={componentList["newInputButton"]}> Enter</Button>
                    </div>
                </div>

            </div>

            <hr/>

            <div className="container-fluid">
                <QuantityModifier item={componentList["quantityModifier"]} />
            </div>

            <div className="container-fluid">
                <div className="row pb-4">
                    <div className="col-6">
                        <Button className="btn btn-primary" item={componentList["backButton"]}><i className="fas fa-chevron-left"></i> Back</Button>
                    </div>
                </div>
            </div>
        </>
    );
}
