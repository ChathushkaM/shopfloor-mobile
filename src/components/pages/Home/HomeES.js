import React, {useState} from 'react';
import {generateHomeDisplay} from './HomeDS';
import config from './HomeCS';
import API from '../../../api/API';

const Home = () => {
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender;

    config["sampleButton"].event.onClick = openSamplePopUp;
    config["buttonCloseSamplePopUp"].event.onClick = closeSamplePopUp;
    config["resetButton"].event.onClick = resetForm;
    // config["inputDate"].event.onChange = __getDailyShiftTeams;
    config["inputTeam"].event.onEnterKey = __getDailyShiftTeams;
    config["inputShift"].event.onEnterKey = __getTeamShift;

    config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    config["CONTROL_CENTER"].event.onNew = handleNew;
    config["CONTROL_CENTER"].event.onDelete = handleDelete;
    config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/

    /*********************************************************/

    function handleChange(event) {
        return onChange(event);
    }

    function handlePopulate(event, callback) {
        return onPopulate(event, callback);
    }

    function handleNew(event) {
        return onNew();
    }

    function handleDelete(event) {
        return onDelete();
    }

    function handleRefresh(event) {
        return onRefresh();
    }

    function handleSave(event, beforeSaveArr) {
        if (beforeSaveArr.action === "NEW") {
            onSaveNew(beforeSaveArr)
        } else if (beforeSaveArr.action === "DELETE") {
            onSaveDelete(beforeSaveArr)
        } else if (beforeSaveArr.action === "MODIFY") {
            onSaveModify(beforeSaveArr)
        }
        let afterSaveArr = {...beforeSaveArr.data}
        return afterSaveArr
    }

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    //Change date format
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    // Get Details
    async function __getDetails(key, distinct, select, where, relations, orderby, limit) {
        try {
            const apiRequest = {
                [key]: {
                    "distinct": distinct,
                    "select": select,
                    "where": where,
                    "relations": relations,
                    "orderby": orderby,
                    "limit": limit
                }
            };
            const getDetails = await API.post(`searchByParameters`, apiRequest);
            const details = getDetails.data;
            return details;
        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error.response);
            return "Error";
        }
    }

    //Get daily shift teams
    async function __getDailyShiftTeams(date) {
        try {
            let dropdownOptions = [{value: "", text: " - Select Team - "}]
            const key = "DailyShiftTeam";
            const distinct = false;
            const select = ["*"];
            const where = [{
                "field-name": "current_date",
                "operator": "=",
                "value": formatDate(date)
            }];
            const relations = ["team"];
            const orderby = "created_at:desc";
            const limit = 1000;
            const dataList = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            console.log(dataList);
            if (dataList && dataList !== "Error" && dataList[0].DailyShiftTeam.length > 0) {
                dataList[0].DailyShiftTeam.forEach(data => {
                    dropdownOptions.push({"value": data.id, "text": data.team.code})
                });
            }
            config["inputTeamDropDown"].setOptions(dropdownOptions);

        } catch (error) {
            console.log("***********Input Team Dropdown Error Error**********");
            console.log(error.response);
        }

    }

    //Get team shifts
    async function __getTeamShift() {
        try {
            let dropdownOptions = [{value: "", text: " - Select Team - "}]
            let id = document.getElementById("inputShift").value; //Need to edit
            const key = "Shift";
            const distinct = false;
            const select = ["*"];
            const where = [{
                "field-name": "id",
                "operator": "=",
                "value": id
            }];
            const relations = [];
            const orderby = "created_at:desc";
            const limit = 1000;
            const dataList = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            console.log(dataList);
            // if (dataList && dataList !== "Error" && dataList[0].ShiftDetail.length >0){
            //     dataList[0].ShiftDetail.forEach(data => {
            //        dropdownOptions.push({"value": data.id, "text": data.shift.id})
            //     });
            // }
            config["inputTeamDropDown"].setOptions(dropdownOptions);

        } catch (error) {
            console.log("***********Input Shift Dropdown Error Error**********");
            console.log(error.response);
        }
    }

    //Reset form
    function resetForm() {
        try {
            let value = "";

            config["inputShift"].setValue(value);
            config["inputTeam"].setValue(value);
            config["inputSlot"].setValue(value);

        } catch (error) {
            console.log(error.response)
        }

    }

    /*********************************************************/
    /********      Framework Public Functions       **********/

    /*********************************************************/

    function openSamplePopUp() {
        config["samplePopUpPage"].showPopUp();
    }

    function closeSamplePopUp() {
        config["samplePopUpPage"].closePopUp();
    }

    function onChange(event) {
        event.preventDefault();
        alert("This is the place where you write CHANGE")
    }

    function onPopulate(event) {
        event.preventDefault();
        alert("This is the place where you write POPULATE")
    }

    function onNew() {
        let dataArray = {};
        //Action handling when NEW buttion clicked...
        alert("This is the place where you write NEW")
        return dataArray
    }

    function onDelete() {
        //Action handling when DELETE buttion clicked...
        alert("This is the place where you write DELETE")
    }

    function onRefresh() {
        //Action handling when REFRESH buttion clicked...
        alert("This is the place where you write REFRESH")
    }

    function onSaveNew(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveModify(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveDelete(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    return generateHomeDisplay(config)
}

export default Home;