import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField, DateField } from '../../../BASE/Components'

export function generateHomeDisplay(componentList, control) {

    return (
        <>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>
            <PopUpPage item={componentList["samplePopUpPage"]} headerText="This is Sample Popup" className="">
                <div className="row p-3">
                    <div className="col-12">
                        <h3>Sample Popup Content</h3>
                    </div>
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Label item={componentList["inputPopUpDropDown"].label} />
                                <DropDown item={componentList["inputPopUpDropDown"]} className="form-control form-control-sm" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <Button className="btn btn-danger float-right" item={componentList["buttonCloseSamplePopUp"]}> Close</Button>
                    </div>
                </div>
            </PopUpPage>
            <div className="row pt-5">
                <div className="col-md-12 pb-4">
                    <div className="card">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputDate"].label} />
                                    <DateField item={componentList["inputDate"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputShift"].label} />
                                    <TextBox item={componentList["inputShift"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputTeam"].label} />
                                    <TextBox item={componentList["inputTeam"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSlot"].label} />
                                    <TextBox item={componentList["inputSlot"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                                    <Button className="btn btn-lg" item={componentList["okButton"]}> OK</Button>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                                    <Button className="btn btn-link" item={componentList["resetButton"]}> Reset</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>)
}
