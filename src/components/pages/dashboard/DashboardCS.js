
let componentListConfig = []

componentListConfig["CONTROL_CENTER"] = {
    objectType: "Controller",
    schema: {
        id: "formSample",
        name: "formSample",
        controllerObject: componentListConfig,
        create: true,
        createAPI: "",
        read: true,
        readAPI: "",
        update: true,
        updateAPI: "",
        delete: false,
        deleteAPI: ""
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSample",
            type: "text",
            visible: true,
            value: "Sample Form"
        },
    },
    state: {
        populated: false,
        modified: false,
        deleted: false,
        new: false
    },
    actions: {
        save: "buttonSave",
        delete: "buttonDelete",
        populate: "buttonPopulate",
        refresh: "buttonRefresh"
    },
    event: {
    }
}

componentListConfig["buttonPopulate"] = {
    objectType: "Button",
    schema: {
        id: "buttonPopulate",
        name: "buttonPopulate",
        type: "submit",
        label: "Populate",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonNew"] = {
    objectType: "Button",
    schema: {
        id: "buttonNew",
        name: "buttonNew",
        type: "submit",
        label: "New",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonSave"] = {
    objectType: "Button",
    schema: {
        id: "buttonSave",
        name: "buttonSave",
        type: "submit",
        label: "Save",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonRefresh"] = {
    objectType: "Button",
    schema: {
        id: "buttonRefresh",
        name: "buttonRefresh",
        type: "submit",
        label: "Clear",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDelete"] = {
    objectType: "Button",
    schema: {
        id: "buttonDelete",
        name: "buttonDelete",
        type: "submit",
        label: "Delete",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputSampleTextBox"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSampleTextBox",
        placeholder: "Sample TextBox",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleTextBox",
            type: "text",
            visible: true,
            value: "Sample TextBox"
        },
        class: ""
    },
    data: {
        sqlcolumn: "sample_textbox",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBundleId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputBundleId",
        placeholder: "Bundle Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBundleId",
            type: "text",
            visible: true,
            value: "Bundle Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "bundle_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputDateDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputDateDropDown",
        placeholder: "Date DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelDateDropDown",
            type: "text",
            visible: true,
            value: "Date"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select Date -" }, { value: "1", text: "Test 1" }, { value: "2", text: "Test 2" }, { value: "3", text: "Test 3" }],
    data: {
        sqlcolumn: "date_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputSlotDropDown",
        placeholder: "Slot DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotDropDown",
            type: "text",
            visible: true,
            value: "Slot"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select Slot -" }, { value: "1", text: "Slot 1" }, { value: "2", text: "Slot 2" }, { value: "3", text: "Slot 3" }],
    data: {
        sqlcolumn: "slot_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSampleDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputSampleDropDown",
        placeholder: "Sample DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleDropDown",
            type: "text",
            visible: true,
            value: "Sample DropDown"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select Sample DropDown Option -" }, { value: "1", text: "Test 1" }, { value: "2", text: "Test 2" }, { value: "3", text: "Test 3" }],
    data: {
        sqlcolumn: "sample_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSampleMoneyField"] = {
    objectType: "MoneyField",
    schema: {
        name: "inputSampleMoneyField",
        placeholder: "Sample MoneyField",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleMoneyField",
            type: "text",
            visible: true,
            value: "Sample MoneyField"
        },
        class: ""
    },
    data: {
        sqlcolumn: "sample_moneyfield",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSampleIntegerField"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputSampleIntegerField",
        placeholder: "Sample IntegerField",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleIntegerField",
            type: "text",
            visible: true,
            value: "Sample IntegerField"
        },
        class: ""
    },
    data: {
        sqlcolumn: "sample_integerfield",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSampleNumberField"] = {
    objectType: "NumberField",
    schema: {
        name: "inputSampleNumberField",
        placeholder: "Sample NumberField",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleNumberField",
            type: "text",
            visible: true,
            value: "Sample NumberField"
        },
        class: ""
    },
    data: {
        sqlcolumn: "sample_numberfield",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSampleTextArea"] = {
    objectType: "TextArea",
    schema: {
        name: "inputSampleTextArea",
        placeholder: "Sample TextArea",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSampleTextArea",
            type: "text",
            visible: true,
            value: "Sample TextArea"
        },
        class: ""
    },
    data: {
        sqlcolumn: "sample_textarea",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShift"] = {
    objectType: "TextArea",
    schema: {
        name: "inputShift",
        placeholder: "Shift",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShift",
            type: "text",
            visible: true,
            value: "Shift"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_textarea",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeam"] = {
    objectType: "TextArea",
    schema: {
        name: "inputTeam",
        placeholder: "Team",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeam",
            type: "text",
            visible: true,
            value: "Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_textarea",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["sampleButton"] = {
    objectType: "Button",
    schema: {
        id: "sampleButton",
        name: "sampleButton",
        type: "submit",
        label: "Sample Button",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["teamButton"] = {
    objectType: "Button",
    schema: {
        id: "teamButton",
        name: "teamButton",
        type: "submit",
        label: "Team",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["shiftButton"] = {
    objectType: "Button",
    schema: {
        id: "shiftButton",
        name: "shiftButton",
        type: "submit",
        label: "Shift",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["slotButton"] = {
    objectType: "Button",
    schema: {
        id: "slotButton",
        name: "slotButton",
        type: "submit",
        label: "Slot",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}


componentListConfig["okButton"] = {
    objectType: "Button",
    schema: {
        id: "okButton",
        name: "okButton",
        type: "submit",
        label: "OK",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["resetButton"] = {
    objectType: "Button",
    schema: {
        id: "resetButton",
        name: "resetButton",
        type: "submit",
        label: "Reset",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["samplePopUpPage"] = {
    objectType: "PopUpPage",
    schema:{
        name: "samplePopUpPage",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
    },
    event:{}
}

componentListConfig["inputPopUpDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputPopUpDropDown",
        placeholder: "PopUp DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelPopUpDropDown",
            type: "text",
            visible: true,
            value: "PopUp DropDown"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select PopUp DropDown Option -" }, { value: "1", text: "Test 1" }, { value: "2", text: "Test 2" }, { value: "3", text: "Test 3" }],
    data: {
        sqlcolumn: "popop_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["buttonCloseSamplePopUp"] = {
    objectType: "Button",
    schema: {
        id: "buttonCloseSamplePopUp",
        name: "buttonCloseSamplePopUp",
        type: "submit",
        label: "Close",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

export default componentListConfig