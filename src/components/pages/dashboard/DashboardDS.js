import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField } from '../../../BASE/Components'

export function generateDashboardDisplay(componentList, control) {
    return (
        <>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>
            <PopUpPage item={componentList["samplePopUpPage"]} headerText="This is Sample Popup" className="">
                <div className="row p-3">
                    <div className="col-12">
                        <h3>Sample Popup Content</h3>
                    </div>
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Label item={componentList["inputPopUpDropDown"].label} />
                                <DropDown item={componentList["inputPopUpDropDown"]} className="form-control form-control-sm" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <Button className="btn btn-danger float-right" item={componentList["buttonCloseSamplePopUp"]}> Close</Button>
                    </div>
                </div>
            </PopUpPage>

            <div class="container-fluid bg-light" >
                    <div className="row">
                        <div className="col-6">
                            <div className="row">
                                <div className="col-6">
                                    <label for="date" class="col-sm-2 col-form-label">Date</label>
                                </div>
                                <div className="col-6">
                                    <input type="text" readonly class="text-box form-control-plaintext" id="staticEmail" value="Aug 27 2020"></input>
                                </div>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="row">
                                <div className="col-6">
                                    <label for="team" class="col-sm-2 col-form-label">Team</label>
                                </div>
                                <div className="col-6">
                                    <input type="text" readonly class="text-box form-control-plaintext" id="staticEmail" value="A"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="row">
                                <div className="col-6">
                                    <label for="shift" class="col-sm-2 col-form-label">Shift</label>
                                </div>
                                <div className="col-6">
                                    <input type="text" readonly class="text-box form-control-plaintext" id="staticEmail" value="Morning"></input>
                                </div>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="row">
                                <div className="col-6">
                                    <label for="slot" class="col-sm-2 col-form-label">Slot</label>
                                </div>
                                <div className="col-6">
                                    <input type="text" readonly class="text-box form-control-plaintext" id="staticEmail" value="1"></input>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div className="form-row">
                <div className="bundle-id col-md-12">
                    <Label item={componentList["inputBundleId"].label} />
                    <IntegerField item={componentList["inputBundleId"]} className="form-control form-control-lg text-center" />
                </div>
            </div>

            <hr id="hr-1" />

            <div class="container-fluid col-md-3">
                <div class="container align-self-center">
                    <div class="row">
                        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary detail-button btn-2">1234676545678</button>
                            <button type="button" class="btn btn-secondary detail-button">M - 20</button>
                        </div>
                        <button type="button" class="close detail-close">
                            <span><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <span class="badge badge-light detail-label">Packing Out</span>
                </div>
                <div class="container align-self-center">
                    <div class="row">
                        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary detail-button btn-2">1234676545678</button>
                            <button type="button" class="btn btn-secondary detail-button">M - 20</button>
                        </div>
                        <button type="button" class="close detail-close">
                            <span><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <span class="badge badge-light detail-label" >Packing Out</span>
                </div>
            </div>

            <hr />
                <div class="container-fluid col-md-3">
                <div class="text-center">
                    <p>Edit tools</p>
                </div>
                    <div className="row-edit-tools">
                        <div class="col-xs-3 col-4 col-lg-6">
                            <Button className="btn btn-lg" item={componentList["teamButton"]}> Team</Button>
                        </div>
                        <div class="col-xs-3 col-4 col-lg-6">
                            <Button className="btn btn-lg" item={componentList["shiftButton"]}> Shift</Button>
                        </div>
                        <div class="col-xs-3 col-4 col-lg-6">
                            <Button className="btn btn-lg" item={componentList["slotButton"]}> Slot</Button>
                        </div>
                    </div>
                </div>

        </>
    );
}
