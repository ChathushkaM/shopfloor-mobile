import React, { useState } from 'react';
import { generateSampleDisplay } from './SampleDS';
import config from './SampleCS';
import API from '../../../api/API';

const Sample = () => {
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender

    config["inputSampleDate"].event.onChange = handleChangeSampleDate;
    config["sampleButton"].event.onClick = openSamplePopUp;
    config["buttonCloseSamplePopUp"].event.onClick = closeSamplePopUp;

    config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    config["CONTROL_CENTER"].event.onNew = handleNew;
    config["CONTROL_CENTER"].event.onDelete = handleDelete;
    config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/
    /*********************************************************/

    function handleChange(event) {
        return onChange(event);
    }

    function handlePopulate(event, callback) {
        return onPopulate(event, callback);
    }

    function handleNew(event) {
        return onNew();
    }

    function handleDelete(event) {
        return onDelete();
    }

    function handleRefresh(event) {
        return onRefresh();
    }

    function handleSave(event, beforeSaveArr) {
        if (beforeSaveArr.action === "NEW") {
            onSaveNew(beforeSaveArr)
        }
        else if (beforeSaveArr.action === "DELETE") {
            onSaveDelete(beforeSaveArr)
        }
        else if (beforeSaveArr.action === "MODIFY") {
            onSaveModify(beforeSaveArr)
        }
        let afterSaveArr = { ...beforeSaveArr.data }
        return afterSaveArr
    }

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    function __formatDateYmd(date) {
        let convertedDate = new Date(date);
        let formattedDate = "";
        let day = convertedDate.getDate();
        let month = convertedDate.getMonth() + 1;
        let year = convertedDate.getFullYear();
        if (day < 10) { day = `0${day}` }
        if (month < 10) { month = `0${month}` }
        formattedDate = `${year}-${month}-${day}`;
        return formattedDate;
    }

    /*********************************************************/
    /********      Framework Public Functions       **********/
    /*********************************************************/

    function handleChangeSampleDate(date){
        config["inputSampleDate"].setDate(__formatDateYmd(date));
        alert(config["inputSampleDate"].data.value)
    }

    function openSamplePopUp() {
        config["samplePopUpPage"].showPopUp();
    }

    function closeSamplePopUp() {
        config["samplePopUpPage"].closePopUp();
    }

    function onChange(event) {
        event.preventDefault();
        alert("This is the place where you write CHANGE")
    }

    function onPopulate(event) {
        event.preventDefault();
        alert("This is the place where you write POPULATE")
    }

    function onNew() {
        let dataArray = {};
        //Action handling when NEW buttion clicked...
        alert("This is the place where you write NEW")
        return dataArray
    }

    function onDelete() {
        //Action handling when DELETE buttion clicked...
        alert("This is the place where you write DELETE")
    }

    function onRefresh() {
        //Action handling when REFRESH buttion clicked...
        alert("This is the place where you write REFRESH")
    }

    function onSaveNew(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveModify(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveDelete(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    return generateSampleDisplay(config)
}

export default Sample;