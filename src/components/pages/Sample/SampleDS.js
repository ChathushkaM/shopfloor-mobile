import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField, DateField } from '../../../BASE/Components'

export function generateSampleDisplay(componentList, control) {

    return (
        <>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>
            <PopUpPage item={componentList["samplePopUpPage"]} headerText="This is Sample Popup" className="">
                <div className="row p-3">
                    <div className="col-12">
                        <h3>Sample Popup Content</h3>
                    </div>
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Label item={componentList["inputPopUpDropDown"].label} />
                                <DropDown item={componentList["inputPopUpDropDown"]} className="form-control form-control-sm" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <Button className="btn btn-danger float-right" item={componentList["buttonCloseSamplePopUp"]}> Close</Button>
                    </div>
                </div>
            </PopUpPage>
            <div className="row pt-5">
                <div className="col-md-12 pb-4">
                    <div className="card">
                        <div className="card-body">
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleTextBox"].label} />
                                    <TextBox item={componentList["inputSampleTextBox"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-2">
                                    <Label item={componentList["inputSampleDate"].label} />
                                    <DateField item={componentList["inputSampleDate"]} dateFormat="yyyy-MM-dd" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleDropDown"].label} />
                                    <DropDown item={componentList["inputSampleDropDown"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleMoneyField"].label} />
                                    <MoneyField item={componentList["inputSampleMoneyField"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleIntegerField"].label} />
                                    <IntegerField item={componentList["inputSampleIntegerField"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleNumberField"].label} />
                                    <NumberField item={componentList["inputSampleNumberField"]} className="form-control form-control-sm" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Label item={componentList["inputSampleTextArea"].label} />
                                    <TextArea item={componentList["inputSampleTextArea"]} className="form-control form-control-sm" style={{ width: "100%", height: "200px" }} />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <Button className="btn btn-success" item={componentList["sampleButton"]}> Sample Button</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>)
}
