//import React, { useState } from 'react';
import React, { useEffect, useState } from 'react';
import { generateDashboardDisplay } from './FgScanDS';
import config from './FgScanCS';
import API from '../../../api/API';
import { getUser } from '../../../utils/Common';
import { getScanDate, getScanShift, getScanTeam, getDailyShiftTeamId, getScanSlot, getScanId } from '../../../utils/Common';

const FgScan = () => {
    const user = getUser();
    var deleteEvent;
    var deleteId = 0;
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    useEffect(() => {
       // get_job_cards();
        //config["inputJobCardDropDown"].setDisabled(true);
    }, []);

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender

     config["inputBundleId"].event.onEnterKey = handleSave;
     config["inputTeam"].event.onEnterKey = handleScanTeam;
     config["inputShift"].event.onEnterKey = handleScanShift;
     config["inputSlot"].event.onEnterKey = handleScanSlot;
     config["inputPopUpDropDown"].event.onChange = handleEntryTypeChange;
     config["buttonDeleteMasterYes"].event.onClick = handleDeleteMasterYes;
     config["buttonDeleteMasterNo"].event.onClick = handleDeleteMasterNo;
     config["quantityModifier"].event.onCloseClick = handleQuantityModifierClose
    // config["quantityModifier"].event.onQuantityClick = handleQuantityModifierQtyClick


    // config["buttonCloseSamplePopUp"].event.onClick = closeSamplePopUp;

    // config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    // config["CONTROL_CENTER"].event.onNew = handleNew;
    // config["CONTROL_CENTER"].event.onDelete = handleDelete;
    // config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    // config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/
    /*********************************************************/

    // function handleChange(event) {
    //     return onChange(event);
    // }

    // function handlePopulate(event, callback) {
    //     return onPopulate(event, callback);
    // }

    // function handleNew(event) {
    //     return onNew();
    // }

    // function handleDelete(event) {
    //     return onDelete();
    // }

    // function handleRefresh(event) {
    //     return onRefresh();
    // }

    // function handleSave(event, beforeSaveArr) {
    //     if (beforeSaveArr.action === "NEW") {
    //         onSaveNew(beforeSaveArr)
    //     }
    //     else if (beforeSaveArr.action === "DELETE") {
    //         onSaveDelete(beforeSaveArr)
    //     }
    //     else if (beforeSaveArr.action === "MODIFY") {
    //         onSaveModify(beforeSaveArr)
    //     }
    //     let afterSaveArr = { ...beforeSaveArr.data }
    //     return afterSaveArr
    //}

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    /*********************************************************/
    /********      Framework Public Functions       **********/
    /*********************************************************/

    // function openSamplePopUp() {
    //     config["samplePopUpPage"].showPopUp();
    // }

    // function closeSamplePopUp() {
    //     config["samplePopUpPage"].closePopUp();
    // }

    // function onChange(event) {
    //     event.preventDefault();
    //     alert("This is the place where you write CHANGE")
    // }

    // function onPopulate(event) {
    //     event.preventDefault();
    //     alert("This is the place where you write POPULATE")
    // }

    // function onNew() {
    //     let dataArray = {};
    //     //Action handling when NEW buttion clicked...
    //     alert("This is the place where you write NEW")
    //     return dataArray
    // }

    // function onDelete() {
    //     //Action handling when DELETE buttion clicked...
    //     alert("This is the place where you write DELETE")
    // }

    // function onRefresh() {
    //     //Action handling when REFRESH buttion clicked...
    //     alert("This is the place where you write REFRESH")
    // }

    // function onSaveNew(dataArr) {
    //     let resultArr = {}
    //     //Your code goes here...
    //     return resultArr
    // }

    // function onSaveModify(dataArr) {
    //     let resultArr = {}
    //     //Your code goes here...
    //     return resultArr
    // }

    // function onSaveDelete(dataArr) {
    //     let resultArr = {}
    //     //Your code goes here...
    //     return resultArr
    // }

    
    async function get_job_cards() {
        try {
            let dropdownOptions = [{ value: "", text: "- Select Shipment Mode -" }];

             const dataList = await API.post(`jobCards/getJobcards`);

            if (dataList.data.length > 0) {
                dataList.data.forEach((data, index) => (
                    dropdownOptions.push({ "value": data.id, "text": data.id })
                ));
            } 
            // dropdownOptions.push(
            //     { "value": "Sea", "text": "Sea" },
            //     { "value": "Air", "text": "Air" },
            //     { "value": "Currier", "text": "Currier" }
            // )

            //config["inputJobCardDropDown"].setOptions(dropdownOptions);

        } catch (error) {
            console.log("***********GetShipmentModeDropdownOptions Error**********");
            console.log(error.response);
        }
    }
    
    function handleEntryTypeChange(){
       // config["inputJobCardDropDown"].setValue("");
        let entry_type = config["inputPopUpDropDown"].data.value;
        if(entry_type == 1){
           // config["inputJobCardDropDown"].setDisabled(true);
        }
        else if( entry_type == 2){
           // config["inputJobCardDropDown"].setDisabled(false);
        }
        __setScanningList();
        
    }


    function handleSave(){
        
        let box = config["inputBundleId"].data.value;
        let entry_type = config["inputPopUpDropDown"].data.value;
        
        if(entry_type == ""){
            alert("Plrease Select Entry Type");
            
        }
        else if(!(parseInt(box) > 0)){
            alert("Enter Valid Box Id")
        }
        else if(config["inputTeamId"].data.value == "" && entry_type == 2){
            alert("Please Enter Team Number")
        }
        // else if(config["inputJobCardDropDown"].data.value == "" && entry_type == 2){
        //     alert("Please Select Job Card")
        // }
        else{
             __updateBoxScanning();
        }
    }

    async function __updateBoxScanning(){
        __setScanningList();
        document.getElementById("inputBundleId").disabled = true;
        try{
        const apiRequest = {
            "box_id": config["inputBundleId"].data.value,
            "team_id":config["inputTeamId"].data.value,
            "entry_type":config["inputPopUpDropDown"].data.value,
            "shift":config["inputShiftId"].data.value,
            "slot":config["inputSlotId"].data.value,
            "user_id":user.name
            //"job_card":config["inputJobCardDropDown"].data.value
        }

        let updateData = await API.post(`packingLists/updateBoxScanning`, apiRequest);
        
        if(updateData.status == "200"){
            document.getElementById("inputBundleId").disabled = false;
            __setScanningList();
            alert("Success");
             config["inputBundleId"].setValue("");
             document.getElementById("inputBundleId").focus();
        }
       else if(updateData.status == "205"){
        document.getElementById("inputBundleId").disabled = false;
            alert("Packing List Not In WareHouse");
        }
        else if(updateData.status == "206"){
            document.getElementById("inputBundleId").disabled = false;
            alert("Box Already Sacaned");
        }
        else if(updateData.data.status == "invalid_box"){
            document.getElementById("inputBundleId").disabled = false;
            alert("Invalid Box");
        }
        else if(updateData.data.status == "Insufficient_Qty"){
            document.getElementById("inputBundleId").disabled = false;
            alert(updateData.data.Msg);
        }
        
        else{
            document.getElementById("inputBundleId").disabled = false;
            alert(updateData.data.Msg);
        }
    }
    catch(error){
        document.getElementById("inputBundleId").disabled = false;
        console.log(error, " Update Box Scanning Error Message ");
        alert(error)
    }
    document.getElementById("inputBundleId").disabled = false;
    document.getElementById("inputBundleId").focus();
    }

    async function __setScanningList() {
        let list = [];
        try {
            const apiRequest = {
               
                "entry_type":config["inputPopUpDropDown"].data.value,
                "shift":config["inputShiftId"].data.value,
                "slot":config["inputSlotId"].data.value,
                "team_id":config["inputTeamId"].data.value
                
            }

            let getDetails = await API.post(`packingLists/getFgScanningList`, apiRequest);
            
            if (getDetails && getDetails !== "Error" && getDetails.data.length > 0) {
                
                
                getDetails.data.forEach(data => {
                    
                    let size ="";
                    let qty = "";
                    Object.keys(data.json).forEach(function(key) {
                        if(parseInt(data.json[key]) > 0){
                            qty += data.json[key]+" ";
                            size += key+" ";
                        }
                      
                      });
                      

                      list.push({ 
                        id: data.id, 
                        scanId: data.id,
                        quantity: qty, 
                        fullQuantity: 0,
                        displayText: size
                    });

                });
            }
        } catch (error) {
            alert(error.response.data.message)
            console.log(error.response);
        }

        config["quantityModifier"].setList(list);
    }

    async function handleScanTeam(event) {
        event.preventDefault();
        try {
            const teamId = config["inputTeam"].data.value;
            const teamDetail = await __getTeamDetails(teamId);

            if (teamDetail !== "") {
                config["inputTeamId"].setValue(teamId);
                config["inputTeamName"].setValue(teamDetail);
                config["inputTeam"].setValue(" ");
            } else {
                config["inputTeamId"].setValue("");
                config["inputTeamName"].setValue("");
                config["inputTeam"].setValue(" ");
                document.getElementById("inputTeam").focus();
                alert("Invalid Team Id");
                //config["CONTROL_CENTER"].promptWarningMessage("Invalid Team Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function __getTeamDetails(teamId) {
        let teamDetail = "";
        try {
            const key = "Team";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": teamId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Team.length > 0) {
                const details = getDetails[0].Team[0];
                teamDetail = details.description;
            }
        } catch (error) {
            console.log(error.response);
        }
        return teamDetail;
    }

    async function __getDetails(key, distinct, select, where, relations, orderby, limit) {
        try {
            const apiRequest = {
                [key]: {
                    "distinct": distinct,
                    "select": select,
                    "where": where,
                    "relations": relations,
                    "orderby": orderby,
                    "limit": limit
                }
            };

            const getDetails = await API.post(`searchByParameters`, apiRequest);
            const details = getDetails.data;

            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error.response);
            return "Error";
        }
    }

    async function handleScanShift(event) {
        event.preventDefault();
        try {
            const shiftId = config["inputShift"].data.value;
            const shiftDetail = await __getShiftDetails(shiftId);

            if (shiftDetail !== "") {
                config["inputShiftId"].setValue(shiftId);
                config["inputShiftName"].setValue(shiftDetail);
                config["inputShift"].setValue(" ");
                document.getElementById("inputTeam").focus();
            } else {
                config["inputShiftId"].setValue("");
                config["inputShiftName"].setValue("");
                config["inputShift"].setValue(" ");
                document.getElementById("inputShift").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Shift Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function __getShiftDetails(shiftId) {
        let shiftDetail = "";
        try {
            const key = "Shift";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": shiftId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Shift.length > 0) {
                const details = getDetails[0].Shift[0];
                shiftDetail = details.name;
            }
        } catch (error) {
            console.log(error.response);
        }
        return shiftDetail;
    }

    async function handleScanSlot(event) {
        event.preventDefault();
        try {
            const slotId = config["inputSlot"].data.value;
            const slotDetail = await __getSlotDetails(slotId);

            if (slotDetail !== "") {
                config["inputSlotId"].setValue(slotId);
                __setScanningList();
                config["inputSlotName"].setValue(slotDetail);
                config["inputSlot"].setValue(" ");
                document.getElementsByClassName("main-button")[0].focus();
            } else {
                config["inputSlotId"].setValue("");
                config["inputSlotName"].setValue("");
                config["inputSlot"].setValue(" ");
                document.getElementById("inputSlot").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Slot Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function __getSlotDetails(slotId) {
        let slotDetail = "";
        try {
            const key = "DailyScanningSlot";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "seq_no",
                    "operator": "=",
                    "value": slotId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].DailyScanningSlot.length > 0) {
                const details = getDetails[0].DailyScanningSlot[0];
                slotDetail = details.seq_no;
            }
        } catch (error) {
            console.log(error.response);
        }
        return slotDetail;
    }

    async function handleQuantityModifierClose(event, id){
        deleteEvent = event;
        deleteId = id;
        config["deleteMasterPopUp"].showPopUp();
 
    }


    async function handleDeleteMasterYes(){
        var event = deleteEvent;
        var id = deleteId;
        if(id !== ""){
            if(config["inputPopUpDropDown"].data.value == ""){
                alert("Please Select Entry Type");
            }
            else{
                try {
                    const apiRequest = {
                       
                        "id":id,
                        'entry_type':config["inputPopUpDropDown"].data.value,
                        'username':user.name                                            
                    }
        
                    let deleteStatus = await API.post(`packingLists/deleteFgScanningList`, apiRequest);
                    if(deleteStatus.status == "200"){
                        alert("Delete successfully");
                        __setScanningList();
                    }
                    else{
                        alert("Error");
                    }
                    
                } catch (error) {
                    console.log(error.response);
                }
            }
        }

        deleteId = 0;
        deleteEvent = null;
        config["deleteMasterPopUp"].closePopUp();

    }
    
    function handleDeleteMasterNo() {
        config["deleteMasterPopUp"].closePopUp();
    }



    return generateDashboardDisplay(config)
}
    
export default FgScan;