import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField ,QuantityModifier} from '../../../BASE/Components'

export function generateDashboardDisplay(componentList, control) {
    return (
        <>
            <PopUpPage item={componentList["deleteMasterPopUp"]} headerText="Confirm Delete" className="">
                <div className="row p-4 float-right">
                    <Button className="btn btn-danger mr-2" item={componentList["buttonDeleteMasterYes"]}> Yes</Button>
                    <Button className="btn btn-info" item={componentList["buttonDeleteMasterNo"]}> No</Button>
                </div>
            </PopUpPage>
            {/* <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>
            <PopUpPage item={componentList["samplePopUpPage"]} headerText="This is Sample Popup" className="">
                <div className="row p-3">
                    <div className="col-12">
                        <h3>Sample Popup Content</h3>
                    </div>
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <Label item={componentList["inputPopUpDropDown"].label} />
                               
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <Button className="btn btn-danger float-right" item={componentList["buttonCloseSamplePopUp"]}> Close</Button>
                    </div>
                </div>
            </PopUpPage> */}

            <div className="form-row" style={{marginTop:"20px"}}>
                <div className="bundle-id col-md-12">
                    <Label item={componentList["inputPopUpDropDown"].label} />
                    <DropDown item={componentList["inputPopUpDropDown"]} className="form-control form-control-lg" />
                </div>
            </div>
            <div className="form-row" style={{marginTop:"20px"}}>
                {/* <div className="bundle-id col-md-12">
                    <Label item={componentList["inputJobCardDropDown"].label} />
                    <DropDown item={componentList["inputJobCardDropDown"]} className="form-control form-control-lg" />
                </div> */}
            </div>
            <div className="form-row">
                    <div className="form-group col-md-12 " style={{textAlign:"center",marginTop:"40px"}}>
                        <Label item={componentList["inputShift"].label} />
                        <IntegerField item={componentList["inputShift"]} className="form-control form-control-lg" />
                        <TextBox item={componentList["inputShiftId"]} />
                    </div>
                    <div className="form-group col-md-12">
                        <TextBox item={componentList["inputShiftName"]} className="form-control-plaintext display-text" />
                    </div>
                </div>

            <div className="form-row text-left">
                <div className=" col-md-12" style={{textAlign:"center"}}>
                    <Label item={componentList["inputTeam"].label}/>
                    <IntegerField item={componentList["inputTeam"]} className="form-control form-control-lg" />
                    <TextBox item={componentList["inputTeamId"]} />
                </div>
            </div>

            <div className="form-row">   
                    <TextBox item={componentList["inputTeamName"]} className="form-control-plaintext display-text" /> 
            </div>

            <div className="form-row">
                <div className="form-group col-md-12 " style={{textAlign:"center"}}>
                    <Label item={componentList["inputSlot"].label} />
                    <IntegerField item={componentList["inputSlot"]} className="form-control form-control-lg" />
                    <TextBox item={componentList["inputSlotId"]} />
                </div>
                <div className="form-group col-md-12">
                    <TextBox item={componentList["inputSlotName"]} className="form-control-plaintext display-text" />
                </div>
            </div>

            <div className="form-row text-left">
                <div className="bundle-id col-md-12">
                    <Label item={componentList["inputBundleId"].label} className="text-left"/>
                    <IntegerField item={componentList["inputBundleId"]} className="form-control form-control-lg" />
                </div>
            </div>

            <div className="container-fluid col-md-12" style={{marginTop:"30px"}}>
                <QuantityModifier item={componentList["quantityModifier"]} />
            </div>



            <hr id="hr-1" />

            {/* <div class="container-fluid col-md-3">
                <div class="container align-self-center">
                    <div class="row">
                        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary detail-button btn-2">1234676545678</button>
                            <button type="button" class="btn btn-secondary detail-button">M - 20</button>
                        </div>
                        <button type="button" class="close detail-close">
                            <span><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <span class="badge badge-light detail-label">Packing Out</span>
                </div>
                <div class="container align-self-center">
                    <div class="row">
                        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary detail-button btn-2">1234676545678</button>
                            <button type="button" class="btn btn-secondary detail-button">M - 20</button>
                        </div>
                        <button type="button" class="close detail-close">
                            <span><i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <span class="badge badge-light detail-label" >Packing Out</span>
                </div>
            </div> */}

            <hr />



            {/* Button */}
                {/* <div class="container-fluid col-md-3">
                <div class="text-center">
                    
                </div>
                    <div className="row-edit-tools">

                        <div class="col-xs-3 col-4 col-lg-6">
                            <Button className="btn btn-lg" item={componentList["saveButton"]}> Save</Button>
                        </div>

                    </div>
                </div> */}

        </>
    );
}
