
let componentListConfig = []

componentListConfig["CONTROL_CENTER"] = {
    objectType: "Controller",
    schema: {
        id: "formSample",
        name: "formSample",
        controllerObject: componentListConfig,
        create: true,
        createAPI: "",
        read: true,
        readAPI: "",
        update: true,
        updateAPI: "",
        delete: false,
        deleteAPI: ""
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSample",
            type: "text",
            visible: true,
            value: "Sample Form"
        },
    },
    state: {
        populated: false,
        modified: false,
        deleted: false,
        new: false
    },
    actions: {
        save: "buttonSave",
        delete: "buttonDelete",
        populate: "buttonPopulate",
        refresh: "buttonRefresh"
    },
    event: {
    }
}

// componentListConfig["buttonPopulate"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonPopulate",
//         name: "buttonPopulate",
//         type: "submit",
//         label: "Populate",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["buttonNew"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonNew",
//         name: "buttonNew",
//         type: "submit",
//         label: "New",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["buttonSave"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonSave",
//         name: "buttonSave",
//         type: "submit",
//         label: "Save",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["buttonRefresh"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonRefresh",
//         name: "buttonRefresh",
//         type: "submit",
//         label: "Clear",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["buttonDelete"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonDelete",
//         name: "buttonDelete",
//         type: "submit",
//         label: "Delete",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["inputSampleTextBox"] = {
//     objectType: "TextBox",
//     schema: {
//         name: "inputSampleTextBox",
//         placeholder: "Sample TextBox",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleTextBox",
//             type: "text",
//             visible: true,
//             value: "Sample TextBox"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "sample_textbox",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }



// componentListConfig["inputDateDropDown"] = {
//     objectType: "DropDown",
//     schema: {
//         name: "inputDateDropDown",
//         placeholder: "Date DropDown",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelDateDropDown",
//             type: "text",
//             visible: true,
//             value: "Date"
//         },
//         class: ""
//     },
//     options: [{ value: "", text: "- Select Date -" }, { value: "1", text: "Test 1" }, { value: "2", text: "Test 2" }, { value: "3", text: "Test 3" }],
//     data: {
//         sqlcolumn: "date_dropdown",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSlotDropDown"] = {
//     objectType: "DropDown",
//     schema: {
//         name: "inputSlotDropDown",
//         placeholder: "Slot DropDown",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSlotDropDown",
//             type: "text",
//             visible: true,
//             value: "Slot"
//         },
//         class: ""
//     },
//     options: [{ value: "", text: "- Select Slot -" }, { value: "1", text: "Slot 1" }, { value: "2", text: "Slot 2" }, { value: "3", text: "Slot 3" }],
//     data: {
//         sqlcolumn: "slot_dropdown",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSampleDropDown"] = {
//     objectType: "DropDown",
//     schema: {
//         name: "inputSampleDropDown",
//         placeholder: "Sample DropDown",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleDropDown",
//             type: "text",
//             visible: true,
//             value: "Sample DropDown"
//         },
//         class: ""
//     },
//     options: [{ value: "", text: "- Select Sample DropDown Option -" }, { value: "1", text: "Test 1" }, { value: "2", text: "Test 2" }, { value: "3", text: "Test 3" }],
//     data: {
//         sqlcolumn: "sample_dropdown",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSampleMoneyField"] = {
//     objectType: "MoneyField",
//     schema: {
//         name: "inputSampleMoneyField",
//         placeholder: "Sample MoneyField",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleMoneyField",
//             type: "text",
//             visible: true,
//             value: "Sample MoneyField"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "sample_moneyfield",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSampleIntegerField"] = {
//     objectType: "IntegerField",
//     schema: {
//         name: "inputSampleIntegerField",
//         placeholder: "Sample IntegerField",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleIntegerField",
//             type: "text",
//             visible: true,
//             value: "Sample IntegerField"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "sample_integerfield",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSampleNumberField"] = {
//     objectType: "NumberField",
//     schema: {
//         name: "inputSampleNumberField",
//         placeholder: "Sample NumberField",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleNumberField",
//             type: "text",
//             visible: true,
//             value: "Sample NumberField"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "sample_numberfield",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputSampleTextArea"] = {
//     objectType: "TextArea",
//     schema: {
//         name: "inputSampleTextArea",
//         placeholder: "Sample TextArea",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelSampleTextArea",
//             type: "text",
//             visible: true,
//             value: "Sample TextArea"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "sample_textarea",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputShift"] = {
//     objectType: "TextArea",
//     schema: {
//         name: "inputShift",
//         placeholder: "Shift",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelShift",
//             type: "text",
//             visible: true,
//             value: "Shift"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "shift_textarea",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["inputTeam"] = {
//     objectType: "TextArea",
//     schema: {
//         name: "inputTeam",
//         placeholder: "Team",
//         type: "text",
//         length: 100,
//         showLabel: true,
//         visible: true,
//         insertable: true,
//         updateAllowed: true,
//         mandetory: true,
//         searchable: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     label: {
//         objectType: "Label",
//         schema: {
//             name: "labelTeam",
//             type: "text",
//             visible: true,
//             value: "Team"
//         },
//         class: ""
//     },
//     data: {
//         sqlcolumn: "team_textarea",
//         oldValue: "",
//         value: ""
//     },
//     class: "",
//     event: {}
// }

// componentListConfig["sampleButton"] = {
//     objectType: "Button",
//     schema: {
//         id: "sampleButton",
//         name: "sampleButton",
//         type: "submit",
//         label: "Sample Button",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["teamButton"] = {
//     objectType: "Button",
//     schema: {
//         id: "teamButton",
//         name: "teamButton",
//         type: "submit",
//         label: "Team",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }



// componentListConfig["slotButton"] = {
//     objectType: "Button",
//     schema: {
//         id: "slotButton",
//         name: "slotButton",
//         type: "submit",
//         label: "Slot",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }


// componentListConfig["okButton"] = {
//     objectType: "Button",
//     schema: {
//         id: "okButton",
//         name: "okButton",
//         type: "submit",
//         label: "OK",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["resetButton"] = {
//     objectType: "Button",
//     schema: {
//         id: "resetButton",
//         name: "resetButton",
//         type: "submit",
//         label: "Reset",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

// componentListConfig["samplePopUpPage"] = {
//     objectType: "PopUpPage",
//     schema:{
//         name: "samplePopUpPage",
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     data: {
//         sqlcolumn: "",
//         oldValue: "",
//         value: "",
//     },
//     event:{}
// }

componentListConfig["inputPopUpDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputPopUpDropDown",
        placeholder: "PopUp DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelPopUpDropDown",
            type: "text",
            visible: true,
            value: "Entry Type"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select Entry Option -" }, { value: "1", text: "Export" }, { value: "2", text: "Into Wh" }],
    data: {
        sqlcolumn: "popop_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputJobCardDropDown"] = {
    objectType: "DropDown",
    schema: {
        name: "inputJobCardDropDown",
        placeholder: "JobCard DropDown",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelJobCardDropDown",
            type: "text",
            visible: true,
            value: "Job Card"
        },
        class: ""
    },
    options: [{ value: "", text: "- Select JobCard Option -" }],
    data: {
        sqlcolumn: "jobcard_dropdown",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBundleId"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputBundleId",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBundleId",
            type: "text",
            visible: true,
            value: "Box ID"
        },
        class: ""
    },
    data: {
        sqlcolumn: "box_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeam"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputTeam",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeam",
            type: "text",
            visible: true,
            value: "Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamId",
        placeholder: "Team Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamId",
            type: "text",
            visible: false,
            value: "Team Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamName",
            type: "text",
            visible: true,
            value: "Team Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["saveButton"] = {
    objectType: "Button",
    schema: {
        id: "savetButton",
        name: "savetButton",
        type: "submit",
        label: "Submit",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputShift"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputShift",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShift",
            type: "text",
            visible: true,
            value: "Shift"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftId",
        placeholder: "Shift Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftId",
            type: "text",
            visible: false,
            value: "Shift Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftName",
            type: "text",
            visible: true,
            value: "Shift Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlot"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputSlot",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlot",
            type: "text",
            visible: true,
            value: "Slot"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotId",
        placeholder: "Slot Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotId",
            type: "text",
            visible: false,
            value: "Slot Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotName",
            type: "text",
            visible: true,
            value: "Slot Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["deleteMasterPopUp"] = {
    objectType: "PopUpPage",
    schema:{
        name: "deleteMasterPopUp",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
    },
    event:{}
}

componentListConfig["buttonDeleteMasterYes"] = {
    objectType: "Button",
    schema: {
        id: "buttonDeleteMasterYes",
        name: "buttonDeleteMasterYes",
        type: "submit",
        label: "Yes",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDeleteMasterNo"] = {
    objectType: "Button",
    schema: {
        id: "buttonDeleteMasterNo",
        name: "buttonDeleteMasterNo",
        type: "submit",
        label: "No",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["quantityModifier"] = {
    objectType: "QuantityModifier",
    schema:{
        name: "quantityModifier",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
        quantityList: [],
    },
    event:{}
}

// componentListConfig["buttonCloseSamplePopUp"] = {
//     objectType: "Button",
//     schema: {
//         id: "buttonCloseSamplePopUp",
//         name: "buttonCloseSamplePopUp",
//         type: "submit",
//         label: "Close",
//         disabled: false,
//         visible: true,
//         dataSourceController: componentListConfig["CONTROL_CENTER"]
//     },
//     event: {}
// }

export default componentListConfig