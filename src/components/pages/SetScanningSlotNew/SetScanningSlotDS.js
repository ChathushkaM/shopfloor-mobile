import React from 'react'
import { TextBox, DropDown, Label, TextArea, Tab, TabPage, Button, Grid, GridBody, GridHeader, CollapsableText, DualStateSelector, ControlCenter, NewButton, SaveButton, RefreshButton, DeleteButton, PopulateButton, CheckBox, MessageListNavigator, SelectedListItem, NonSelectedListItem, AvatarImg, MessageHeader, MessageText, AttachmentList, Attachment, AttachmentName, AttachmentCloseBtn, AvatarList, Messenger, MessageHistrory, MyMessageFormatter, MessageAction, MessageTime, ReceivedMessageFormatter, ChatMessageBtton, FileSelector, PopUpPage, MoneyField, AdvanceSearch, AdvanceSearchGrid, AdvanceSearchButton, IntegerField, NumberField, DateField,QuantityModifier } from '../../../BASE/Components'

export function generateSetScanningSlotDisplay(componentList, control) {

    return (
        <>
            <div className="loading" id="spinner" style={{ display: "none" }}>Loading&#8230;</div>
            <ControlCenter item={componentList["CONTROL_CENTER"]}></ControlCenter>

            <br></br>
            {/* <br></br> */}
            <div className="container-fluid py-3">
                <div className="row">
                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className=" col-8 p-0">
                            <Label item={componentList["inputDate"].label} />
                                <DateField item={componentList["inputDate"]} dateFormat="yyyy-MM-dd" />
                            </div>
                        </div>
                    </div>
                    

                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className="col-8 p-0">
                                <Label item={componentList["inputShift"].label} />
                                <IntegerField item={componentList["inputShift"]} className="form-control form-control-sm" />
                                <TextBox item={componentList["inputShiftId"]} />
                            </div>
                            <div className="col-3 p-0 mt-4 pl-3" style={{overflow:"visible"}}>
                                <TextBox item={componentList["inputShiftName"]} className="form-control-plaintext display-text" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className=" col-8 p-0">
                                <Label item={componentList["inputTeam"].label} />
                                <IntegerField item={componentList["inputTeam"]} className="form-control form-control-sm" />
                                <TextBox item={componentList["inputTeamId"]} />
                            </div>
                            <div className=" col-3 p-0 mt-4 pl-2" style={{overflow:"visible"}}>
                                <TextBox item={componentList["inputTeamName"]} className="form-control-plaintext display-text" />
                            </div>
                        </div>
                    </div>
                    

                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className="col-8 p-0">
                                <Label item={componentList["inputSlot"].label} />
                                <IntegerField item={componentList["inputSlot"]} className="form-control form-control-sm" />
                                <TextBox item={componentList["inputSlotId"]} />
                            </div>
                        <div className="col-3 p-0 mt-4 pl-3" style={{overflow:"visible"}}>
                                <TextBox item={componentList["inputSlotName"]} className="form-control-plaintext display-text" />
                            </div>
                        </div>
                    </div>
                </div>

                
                <div className="row">
                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className=" col-8 p-0">
                                <Label item={componentList["inputBox"].label} />
                                <IntegerField item={componentList["inputBox"]} className="form-control form-control-sm" />
                                <TextBox item={componentList["inputBoxId"]} />
                            </div>
                            <div className=" col-3 p-0 mt-4 pl-2" style={{overflow:"visible"}}>
                                <TextBox item={componentList["inputBoxName"]} className="form-control-plaintext display-text" />
                            </div>
                        </div>
                    </div>
                    

                    <div className='col-6'>
                        <div className="form-group row mb-0">
                            <div className="form-check col-8 p-0 mt-4" style= {{marginLeft:"35px"}}>
                                <CheckBox item={componentList["newCheckBox"]} className="form-check-input" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className='col-12'>
                        <div className="form-group row mb-0">
                            <div className="col-12 p-0">
                                <label htmlFor="inputBundleId" className="col-form-label main-label pt-3">Barcode</label>
                                <IntegerField id="inputBundleId" item={componentList["inputBundleId"]} className="form-control form-control-sm text-center" />
                            </div>
                        </div>
                    </div>
                </div>

                <br></br>

                <div className="container-fluid" id="showWhenChecked" style={{borderStyle:"solid" , borderColor:"blue"}}>
                <div className="row">
                    {/* <div className="col-6 text-center"  style={{overflow:"visible"}}>
                        <label htmlFor="inputSecondPackingList" className="col-form-label header-label">Packing List</label>
                        <DropDown item={componentList["inputSecondPackingList"]} className="form-control form-control-sm text-center" />    
                    </div> */}

                    <div className="col-6 text-center"  style={{overflow:"visible"}}>
                        <label htmlFor="viewOriginalQty" className="col-form-label header-label">Original <br></br> Quantity</label>
                        <TextBox item={componentList["viewOriginalQty"]} className="form-control form-control-sm text-center" />    
                    </div>

                    <div className="col-6 text-center"  style={{overflow:"visible"}}>
                        <label htmlFor="viewScanedQty" className="col-form-label header-label">Already Scaned <br></br> Quantity</label>
                        <TextBox item={componentList["viewScanedQty"]} className="form-control form-control-sm text-center" />    
                    </div>
                </div>

                <div className="row">
                    
                    <div className="col-12 text-center"  style={{overflow:"visible"}}>
                        <label htmlFor="inputScannedQty" className="col-form-label header-label">New Scanning Quantity</label>
                        <TextBox item={componentList["inputScannedQty"]} className="form-control form-control-sm text-center" />    
                    </div>
                </div>

                <br>
                </br>

                <div className="row">
                    <div className="col-12 text-center">
                        <Button className="btn btn-primary" item={componentList["newInputButton"]}> Enter</Button>
                    </div>
                </div>

            </div>
            
            <br></br>
                
                <div className="col-12"   style={{overflow:"scroll", padding:"0px", margin:"0px"}} >
                    <Grid item={componentList["gridData"]} className="table table-responsive table-striped table-sm w-100 d-block d-md-table"  style={{padding:"0px", margin:"0px"}}>
                        <div className="master-table-wrp quantity-table">
                            <GridHeader typeName="GridHeader" columns={componentList["gridData"].columns} />
                            <GridBody typeName="GridBody" rows={componentList["gridData"].data} />
                        </div>
                    </Grid>
                </div>

                <div className="row">
                    {/* <div className="col-6 text-center"  style={{overflow:"visible"}}>
                        <Button className="btn btn-primary btn-md main-button" item={componentList["okButton"]}> Save</Button>
                    </div> */}
                    <div className="col-12 text-center"  style={{overflow:"visible"}}>
                        <Button className="btn btn-danger btn-md main-button" item={componentList["resetButton"]}> Delete</Button>
                    </div>
                </div>

                {/* <div className="form-row">
                    <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                        <Button className="btn btn-primary btn-lg main-button" item={componentList["okButton"]}> OK</Button>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group d-flex align-items-center justify-content-center col-md-12">
                        <Button className="btn btn-link" item={componentList["resetButton"]}> Reset</Button>
                    </div>
                </div> */}
            </div>
        </>
    )
}
