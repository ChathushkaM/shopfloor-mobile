import React, { useState } from 'react';
import { generateSetScanningSlotDisplay } from './SetScanningSlotDS';
import config from './SetScanningSlotCS';
import API from '../../../api/API';
import { setScanDate, setScanShift, setScanTeam,setScanPackingList, setScanSlot, setScanId, setDailyShiftTeamId, getScanId, removeScanDate, removeScanShift, removeScanTeam,removeScanPackingList, removeScanSlot, removeScanId, removeDailyShiftTeamId } from '../../../utils/Common';
import { useHistory } from 'react-router-dom';
import { getUser } from '../../../utils/Common';
import { getScanDate, getScanShift, getScanTeam,getScanPackingList, getDailyShiftTeamId, getScanSlot } from '../../../utils/Common';


const SetScanningSlot = () => {
    var showWhenChecked1 = false;
    var scannedPackingListDetails = [];
    var bundleTicketDetailsUni = []; 
    var dailyScanningSlotIdX = 0;
    var dailyShiftTeamIdX = 0;
    const history = useHistory();
    let [rendered, setRendered] = useState(true)

    function reRender() {
        setRendered(!rendered)
    }

    /*********************************************************/
    /********      Framework Action Definitions     **********/
    /*********************************************************/

    config["CONTROL_CENTER"].renderFunction = reRender;

    config["resetButton"].event.onClick = handleResetData;

    config["inputDate"].event.onChange = handleChangeDate;
    config["inputShift"].event.onEnterKey = handleScanShift;
    config["inputTeam"].event.onEnterKey = handleScanTeam;
    config["inputPackingList"].event.onEnterKey = handleScanPackingList;
    config["inputSlot"].event.onEnterKey = handleScanSlot;
    config["inputBox"].event.onEnterKey = handleInputBox;
    config["inputBundleId"].event.onEnterKey = handleInputBundleId;

    config["quantityModifier"].event.onCloseClick = handleQuantityModifierClose
    config["quantityModifier"].event.onQuantityClick = handleQuantityModifierQtyClick

    config["okButton"].event.onClick = handleSetScanningSlot;
    config["newCheckBox"].event.onClick = handleCheckBoxEvent;
    config["newInputButton"].event.onClick = handleEnterButtonPress;

    config["gridData"].event.onChange = handleGridDataChange;

    config["CONTROL_CENTER"].event.onPopulate = handlePopulate;
    config["CONTROL_CENTER"].event.onNew = handleNew;
    config["CONTROL_CENTER"].event.onDelete = handleDelete;
    config["CONTROL_CENTER"].event.onRefresh = handleRefresh;
    config["CONTROL_CENTER"].event.onSave = handleSave;

    /*********************************************************/
    /********       Framework Action Handlers       **********/
    /*********************************************************/

    function handleChange(event) {
        return onChange(event);
    }

    function handlePopulate(event, callback) {
        return onPopulate(event, callback);
    }

    function handleNew(event) {
        return onNew();
    }

    function handleDelete(event) {
        return onDelete();
    }

    function handleRefresh(event) {
        return onRefresh();
    }

    function handleSave(event, beforeSaveArr) {
        if (beforeSaveArr.action === "NEW") {
            onSaveNew(beforeSaveArr)
        } else if (beforeSaveArr.action === "DELETE") {
            onSaveDelete(beforeSaveArr)
        } else if (beforeSaveArr.action === "MODIFY") {
            onSaveModify(beforeSaveArr)
        }
        let afterSaveArr = { ...beforeSaveArr.data }
        return afterSaveArr
    }

    /*********************************************************/
    /********       User Defined Declarations       **********/
    /*********************************************************/

    // Set initila values of Component Schema etc.

    removeScanDate();
    removeScanShift();
    removeScanTeam();
    removeScanPackingList();
    removeScanSlot();
    removeScanId();
    removeDailyShiftTeamId();

    const scanDate = getScanDate();
    const scanShift = getScanShift();
    const scanTeam = getScanTeam();
    const scanSlot = getScanSlot();
    const scanId = getScanId();
    const packing_list = getScanPackingList();
    const dailyShiftTeamId = getDailyShiftTeamId();

    // __setScanningList(scanId, dailyShiftTeamId);

    window.onload = (event) => {
        // __getAutoSlotDetails()
        let today = new Date();
        let hours = today.getHours();
        let minutes = today.getMinutes();

        // config["inputBundleId"].setDisabled(true);

        config["inputDate"].setDate(__formatDateYmd(today));

        if(hours == 7 || hours == 18 || hours == 5){
            if(hours == 7 && minutes >= 30){
                config["inputShift"].setValue(1);
            }
            if(hours == 18 && minutes < 30){
                config["inputShift"].setValue(1);
            }
            if(hours == 18 && minutes >= 30){
                config["inputShift"].setValue(2);
            }
            if(hours == 5 && minutes < 30){
                config["inputShift"].setValue(2);
            }
        }

        if(hours > 7 && hours < 18){
            config["inputShift"].setValue(1);
        }

        if(hours > 18 && hours < 5){
            config["inputShift"].setValue(2);
        }
        if(config["inputShift"].data.value){
            handleScanShift(event);
            // document.getElementById("inputTeam").focus();
        }
        else{
            document.getElementById("inputShift").focus();
        }
        document.getElementById("showWhenChecked").hidden = true;
        document.getElementsByClassName("react-datepicker-wrapper")[0].getElementsByTagName("input")[0].readOnly = true;
    };

    /*********************************************************/
    /********        User Defined Functions         **********/
    /*********************************************************/

    // Get Details
    async function __getDetails(key, distinct, select, where, relations, orderby, limit) {
        try {
            const apiRequest = {
                [key]: {
                    "distinct": distinct,
                    "select": select,
                    "where": where,
                    "relations": relations,
                    "orderby": orderby,
                    "limit": limit
                }
            };

            const getDetails = await API.post(`searchByParameters`, apiRequest);
            const details = getDetails.data;

            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error.response);
            return "Error";
        }
    }

    function handleCheckBoxEvent(event){
        const x = event.target.checked;
        showWhenChecked1 = x;
        console.log(showWhenChecked1 , "Check Box Value");
        if(x){
            document.getElementById("showWhenChecked").hidden = false;
        }
        else{
            document.getElementById("showWhenChecked").hidden = true;
        }
    }

    function handleGridDataChange(event, rowId, colId){
        if(colId == 1){
            config["gridData"].setChecked(true , rowId, "Action");
        }

        if(colId == 10){

        }
    }

    async function handleInputBox(event){
        event.preventDefault();
        try {
            const boxId = event.target.value //config["inputBox"].data.value;
            // console.log(event.target.value , " BOX BOX BOX");
            const boxDetail = await __getBoxDetails(boxId);

            if (boxDetail !== "") {
                config["inputBoxId"].setValue(boxId);
                config["inputBoxName"].setValue(boxDetail);
                config["inputBox"].setValue("");
                document.getElementById("inputBundleId").focus();
                // handleSetScanningSlot(event);
                // document.getElementsByClassName("main-button")[0].click();
            } else {
                config["inputBoxId"].setValue("");
                config["inputBoxName"].setValue("");
                config["inputBox"].setValue("");
                document.getElementById("inputBox").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Box Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleInputBundleId(event){
        console.log(event.target.value , " BUNDLE TICKET ID THAT GOING TO SCAN ");
        try{
            let date = config["inputDate"].data.value;
            let shift = config["inputShiftId"].data.value;
            let team = config["inputTeamId"].data.value;
            let slot = config["inputSlotId"].data.value;
            let box = config["inputBoxId"].data.value;

            console.log(date , " DATE");
            console.log(shift , " shift");
            console.log(team , " team");
            console.log(slot , " slot");
            console.log(box , " box");

            document.getElementById("spinner").style.display = "";
            if(date && shift && team && slot){
                const bundleTicketId = config["inputBundleId"].data.value;
                const key = "BundleTicket";
                const distinct = false;
                const select = ["*"];
                const where = [
                    {
                        "field-name": "id",
                        "operator": "=",
                        "value": bundleTicketId
                    }
                ];
                const relations = ["bundle", "fpo_operation"];
                const orderby = "id:desc";
                const limit = 1000;

                const bundleTicketData = await __getDetails(key, distinct, select, where, relations, orderby, limit);
                console.log(bundleTicketData[0].BundleTicket[0] , " Bundle Ticket Data ");
                bundleTicketDetailsUni = [];
                bundleTicketDetailsUni.push(bundleTicketData[0].BundleTicket[0]);
                
                let xx = bundleTicketData[0].BundleTicket[0].scan_quantity !== null ? bundleTicketData[0].BundleTicket[0].scan_quantity:0;


                if(bundleTicketData[0].BundleTicket[0].fpo_operation.operation == "PK" && bundleTicketData[0].BundleTicket[0].direction == "IN"){
                    console.log(" PACKING IN OP ");
                    
                    if(showWhenChecked1){
                        config["viewOriginalQty"].setValue(0);
                        config["viewScanedQty"].setValue(0);
                        config["inputScannedQty"].setValue(0);
                        config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                        config["viewScanedQty"].setValue(xx);
                        config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - xx);
                    }
                    else{
                        handlePackingInScan(bundleTicketData[0].BundleTicket[0].original_quantity);
                    }
                }
                else{
                    if(showWhenChecked1){
                        config["viewOriginalQty"].setValue(0);
                        config["viewScanedQty"].setValue(0);
                        config["inputScannedQty"].setValue(0);
                        config["viewOriginalQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity);
                        config["viewScanedQty"].setValue(xx);
                        config["inputScannedQty"].setValue(bundleTicketData[0].BundleTicket[0].original_quantity - xx);
                    }
                    else{
                        handleScanBundleTicket();
                    }
                    
                }
            }
            else{
                console.log(" SOME ARE MISSING ");
            }

            document.getElementById("spinner").style.display = "none";
        }
        catch(error){
            document.getElementById("spinner").style.display = "none";
            console.log(error);
        }
    }

    async function handleEnterButtonPress(){
        console.log(bundleTicketDetailsUni[0]);
        
        if(bundleTicketDetailsUni[0].fpo_operation.operation == "PK" && bundleTicketDetailsUni[0].direction == "IN"){
            console.log(" PACKING IN OP ");
            handlePackingInScan(config["inputScannedQty"].data.value);
        }
        else{
            createRecordWhenChecked();
        }
    }

    async function handleQuantityModifierClose(event, id){
        if(id !== ""){
            config["deleteScanRecordPopUp"].showPopUp();
            config["inputDeleteBundleTicketId"].data.value = id;
        }
    }

    async function createRecordWhenChecked(){
        try{
            document.getElementById("spinner").style.display = "";
            const bundleTicketId = config["inputBundleId"].data.value;
            const dailyScanningSlotId = dailyScanningSlotIdX;
            const dailyShiftTeamId = dailyShiftTeamIdX;
            const original_quantity = config["viewOriginalQty"].data.value;
            const scan_quantity = config["inputScannedQty"].data.value;
            var packing_list_id = null;
            const user = getUser();

            const date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            let cDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

            const apiRequest = {
                "bundle_ticket_id": bundleTicketId,
                "user_id":user.name,
                "details" : [{
                "bundle_ticket_id": bundleTicketId,
                "daily_scanning_slot_id": dailyScanningSlotId,
                "daily_shift_team_id" : dailyShiftTeamId,
                "packing_list_id":packing_list_id,
                "bundle_id" : bundleTicketDetailsUni[0].bundle_id,
                "original_quantity" : original_quantity,
                "scan_quantity" : scan_quantity,
                "scan_date_time" : cDate,
                "user_id":user.name
                }]
            }

            var scanBundleTicket1;
            try{
                scanBundleTicket1 = await API.post(`bundleTicketsSecondary/createNewRecordChecked`, apiRequest);
                console.log(scanBundleTicket1);
            }
            catch(error){
                
            }

            if (scanBundleTicket1.status === 200) {
                document.getElementById("spinner").style.display = "none";
                config["CONTROL_CENTER"].promptBaseMessage("Records Updated Successfully", "");
                await __setScanningList(dailyScanningSlotIdX, dailyShiftTeamIdX);
                document.getElementById("inputBundleId").focus();

            } else if (scanBundleTicket1.status === "error"){
                console.log(scanBundleTicket1);
                config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                document.getElementById("spinner").style.display = "none";
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["inputBundleId"].setValue("");
                config["newCheckBox"].setDisabled(false);
                if(document.getElementById("newCheckBox").checked){
                    document.getElementById("newCheckBox").click();
                    document.getElementById("newCheckBox").checked = false;
                }
                document.getElementById("inputBundleId").focus();
            }
            else{
                document.getElementById("spinner").style.display = "none";
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["inputBundleId"].setValue("");
                config["newCheckBox"].setDisabled(false);
                if(document.getElementById("newCheckBox").checked){
                    document.getElementById("newCheckBox").click();
                    document.getElementById("newCheckBox").checked = false;
                }
                document.getElementById("inputBundleId").focus();
            }

            config["inputBundleId"].setValue("");


        }
        catch(error){
            document.getElementById("spinner").style.display = "none";
            config["inputBundleId"].setValue("");
            document.getElementById("inputBundleId").focus();
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    async function handlePackingInScan(qty){
        try{
            document.getElementById("spinner").style.display = "";
            const bundleTicketId = config["inputBundleId"].data.value;
            const boxId = config["inputBoxId"].data.value;
            if(boxId == "" || boxId == 0){
                config["CONTROL_CENTER"].promptWarningMessage("Error", "Please enter box ID hence this is a packing in ticket.");
                return false;
            }
            const user = getUser();

            const date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            let cDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

            const apiRequest = {
                'box_id' : boxId,
                'ticket_id' : bundleTicketId,
                'quantity' : qty,
                'daily_scanning_slot_id' : dailyScanningSlotIdX,
                'daily_shift_team_id' : dailyShiftTeamIdX,
                'scan_date_time' : cDate,
                'user_id' : user.name
            }

            var scanBundleTicket1;
            try{
                scanBundleTicket1 = await API.post(`bundleTickets/newPackingInScanning`, apiRequest);
                console.log(scanBundleTicket1);
            }
            catch(error){
                
            }

            if (scanBundleTicket1.status === 200) {
                document.getElementById("spinner").style.display = "none";
                config["CONTROL_CENTER"].promptBaseMessage("Records Updated Successfully", "");
                await __setScanningList(dailyScanningSlotIdX, dailyShiftTeamIdX);
                document.getElementById("inputBundleId").focus();

            } else if (scanBundleTicket1.status === "error"){
                console.log(scanBundleTicket1);
                config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                document.getElementById("spinner").style.display = "none";
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["inputBundleId"].setValue("");
                config["newCheckBox"].setDisabled(false);
                if(document.getElementById("newCheckBox").checked){
                    document.getElementById("newCheckBox").click();
                    document.getElementById("newCheckBox").checked = false;
                }
                document.getElementById("inputBundleId").focus();
            }
            else{
                document.getElementById("spinner").style.display = "none";
                config["viewOriginalQty"].setValue("");
                config["viewScanedQty"].setValue("");
                config["inputScannedQty"].setValue("");
                config["inputBundleId"].setValue("");
                config["newCheckBox"].setDisabled(false);
                if(document.getElementById("newCheckBox").checked){
                    document.getElementById("newCheckBox").click();
                    document.getElementById("newCheckBox").checked = false;
                }
                document.getElementById("inputBundleId").focus();
            }

            config["inputBundleId"].setValue("");


        }
        catch(error){
            document.getElementById("spinner").style.display = "none";
            console.log(error);
            config["inputBundleId"].setValue("");
            document.getElementById("inputBundleId").focus();
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    async function getPackingListStatus(packListId){
        const key = "PackingList";
        const distinct = false;
        const select = ["status"];
        const where = [
            {
                "field-name": "id",
                "operator": "=",
                "value": packListId
            }
        ];
        const relations = [];
        const orderby = "id:desc";
        const limit = 1000;

        const packStatus = await __getDetails(key, distinct, select, where, relations, orderby, limit);
        console.log(packStatus[0].PackingList[0].status , "STATUS STATUS STATUS");

        if(packStatus[0].PackingList[0].status == "Generated"){
            return true;
        }
        else{
            return false;
        }
    }

    function __getGridDataColumns(){
        let grid1Cols = [];

        grid1Cols["Secondary_ID"] = { objectType: "IntegerField", colIndex: 3, datatype: "text", name: "Secondary_ID", placeholder: "Sec ID", visible: false, editable: false, sqlColumn: "Secondary_ID", style: { textAlign: "center", minWidth: "50px", width: "200px"  } };
        grid1Cols["Bundle_ID"] = { objectType: "IntegerField", colIndex: 4, datatype: "text", name: "Bundle_ID", placeholder: "Bundle ID", visible: true, editable: false, sqlColumn: "Bundle_ID", style: { textAlign: "center", minWidth: "50px", width: "200px"  } };
        grid1Cols["Ticket_ID"] = { objectType: "IntegerField", colIndex: 0, datatype: "text", name: "Ticket_ID", placeholder: "Ticket ID", visible: true, editable: false, sqlColumn: "Ticket_ID", style: { textAlign: "center", minWidth: "50px", width: "200px"  } };
        grid1Cols["Original_Qty"] = { objectType: "IntegerField", colIndex: 2, datatype: "text", name: "Original_Qty", placeholder: "Original Qty", editable: false, sqlColumn: "Original_Qty", style: { textAlign: "center", minWidth: "50px", width: "50px" } }
        grid1Cols["Scanned_Qty"] = { objectType: "IntegerField", colIndex: 1, datatype: "text", name: "Scanned_Qty", placeholder: "Scanned Qty", editable: false, sqlColumn: "Scanned_Qty", style: { textAlign: "center", minWidth: "50px", width: "50px" } }
        grid1Cols["FPO"] = { objectType: "TextBox", colIndex: 5, datatype: "text", name: "FPO", placeholder: "FPO", editable: false, sqlColumn: "FPO", style: { textAlign: "center", minWidth: "50px", width: "200px" } };
        grid1Cols["Operation"] = { objectType: "TextBox", colIndex: 6, datatype: "text", name: "Operation", placeholder: "Operation", editable: false, sqlColumn: "Operation", style: { textAlign: "center", minWidth: "50px", width: "200px" } };
        grid1Cols["Size"] = { objectType: "TextBox", colIndex: 7, datatype: "text", name: "Size", placeholder: "Size", editable: false, sqlColumn: "Size", style: { textAlign: "center", minWidth: "50px", width: "200px" } };
        grid1Cols["Team"] = { objectType: "IntegerField", colIndex: 8, datatype: "text", name: "Team", placeholder: "Team", editable: false, sqlColumn: "Team", style: { textAlign: "center", minWidth: "50px", width: "200px" } };
        grid1Cols["Slot"] = { objectType: "IntegerField", colIndex: 9, datatype: "text", name: "Slot", placeholder: "Slot", visible: true, editable: false, sqlColumn: "Slot", style: { textAlign: "center", minWidth: "50px", width: "200px" } }
        grid1Cols["Action"] = { objectType: "CheckBox", selectAll: false, colIndex: 10, datatype: "text", name: "Action", placeholder: "Select", editable: true, checkedValue: "1", uncheckedValue: "0", sqlColumn: "Action", style: { textAlign: "center", minWidth: "50px", width: "100px", paddingBottom: "4px" } }
        
        return grid1Cols;
    }

    function __resetGridData(){
        let gridColumns = __getGridDataColumns();
        let gridRows = [];
        config["gridData"].setColumns(gridColumns);
        config["gridData"].setData(gridRows);
    }

    async function handleQuantityModifierQtyClick(event, id){
        if(id !== ""){
            config["updateQuantityPopUp"].showPopUp();
            let quantity = await __getScanQuantity(id);
            config["inputUpdateBundleTicketId"].setValue(id);
            config["inputUpdateQuantity"].setValue(quantity);
            document.getElementById("inputUpdateQuantity").focus();
        }
    }

    async function __getScanQuantity(bundleTicketId) {
        let quantity = "";
        try {
            const key = "BundleTicket";
            const distinct = false;
            const select = ["scan_quantity"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": bundleTicketId
                }
            ];
            const relations = [];
            const orderby = "created_at:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);
            
            if (getDetails && getDetails !== "Error" && getDetails[0].BundleTicket.length > 0) {
                quantity = getDetails[0].BundleTicket[0].scan_quantity;
            }
        } catch (error) {
            console.log(error.response);
        }

        return quantity;
    }

    async function __setScanningList(scanningSlotId, dailyShiftTeamId) {
        let list = [];
        const user = getUser();
        try {

            let gridCols = __getGridDataColumns();
            let gridRows = [];


            const getDetails = await __getGridData(scanningSlotId , dailyShiftTeamId);
            console.log(getDetails , " QUANTITY MODIFIYE DETAILS");
            if (getDetails && getDetails !== "Error" && getDetails.data.length > 0) {
                getDetails.data.forEach(data => {
                    gridRows.push({
                        "Secondary_ID" : data.Secondary_ID,
                        "Bundle_ID" :data.Bundle_ID,
                        "Ticket_ID" : data.Ticket_ID,
                        "Original_Qty" : data.Original_Qty,
                        "Scanned_Qty" : data.Scanned_Qty,
                        "FPO" : data.FPO,
                        "Operation" : data.Operation,
                        "Size" : data.Size,
                        "Team" : data.Team,
                        "Slot" : data.Slot,
                        "Action" :"0"
                    });
                });
            }
            config["gridData"].setColumns(gridCols);
            config["gridData"].setData(gridRows);
        } catch (error) {

            console.log(error.response);
        }
        // config["quantityModifier"].setList(list);
    }



    function __formatDateYmd(date) {
        let convertedDate = new Date(date);
        let formattedDate = "";
        let day = convertedDate.getDate();
        let month = convertedDate.getMonth() + 1;
        let year = convertedDate.getFullYear();
        if (day < 10) { day = `0${day}` }
        if (month < 10) { month = `0${month}` }
        formattedDate = `${year}-${month}-${day}`;
        return formattedDate;
    }

    function __formatDateYmdhms(date) {
        let convertedDate = new Date(date);
        let formattedDate = "";
        let day = convertedDate.getDate();
        let month = convertedDate.getMonth() + 1;
        let year = convertedDate.getFullYear();
        let hour = convertedDate.getHours();
        let minutes = convertedDate.getMinutes();
        let seconds = convertedDate.getSeconds();
        if (day < 10) { day = `0${day}` }
        if (month < 10) { month = `0${month}` }

        if (hour < 10) { hour = `0${hour}` }
        if (minutes < 10) { minutes = `0${minutes}` }
        if (seconds < 10) { seconds = `0${seconds}` }
        formattedDate = `${year}-${month}-${day} ${hour}:${minutes}:${seconds}`;
        return formattedDate;
    }

    //Reset form
    function __resetForm() {
        try {
            let value = "";
            config["inputShift"].setValue(value);
            config["inputShiftId"].setValue(value);
            config["inputShiftName"].setValue(value);

            config["inputTeam"].setValue(value);
            config["inputTeamId"].setValue(value);
            config["inputTeamName"].setValue(value);

            config["inputSlot"].setValue(value);
            config["inputSlotId"].setValue(value);
            config["inputSlotName"].setValue(value);

        } catch (error) {
            console.log(error.response)
        }
    }

    async function __getShiftDetails(shiftId) {
        let shiftDetail = "";
        try {
            const key = "Shift";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": shiftId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Shift.length > 0) {
                const details = getDetails[0].Shift[0];
                shiftDetail = details.name;
            }
        } catch (error) {
            console.log(error.response);
        }
        return shiftDetail;
    }

    async function __getTeamDetails(teamId) {
        let teamDetail = "";
        try {
            const key = "Team";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": teamId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].Team.length > 0) {
                const details = getDetails[0].Team[0];
                teamDetail = details.description;
            }
        } catch (error) {
            console.log(error.response);
        }
        return teamDetail;
    }

    async function __getAutoSlotDetails() {
        let autoSlotDetail = "";
        let today = new Date();
        try {
            const apiRequest = {
                'date_long' : __formatDateYmdhms(today),
                'date_short' : __formatDateYmd(today)
            };

            const getDetails =await API.post(`bundleTickets/selectTeamAutomatically`, apiRequest);
            console.log(getDetails , " AUTOMATIC");
            const details = getDetails.data;
            console.log(details , " AUTO DETAILS");
            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error);
            return "Error";
        }
    }

    async function __getGridData(dailyScanningSlot, dailyShiftTeam) {
        try {
            const apiRequest = {
                'daily_scanning_slot_id' : dailyScanningSlot,
                'daily_shift_team_id' : dailyShiftTeam
            };

            const getDetails =await API.post(`bundleTickets/getGridDataBundleScanning`, apiRequest);
            console.log(getDetails , " AUTOMATIC");
            const details = getDetails.data;
            console.log(details , " AUTO DETAILS");
            return details;

        } catch (error) {
            console.log("***********GetDetails Error**********");
            console.log(error);
            return "Error";
        }
    }


    async function __getPackinglistDetails(packing_list_id) {
        let packing_list_details = "";
        try {
            const key = "PackingList";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": packing_list_id
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].PackingList.length > 0) {
                const details = getDetails[0].PackingList[0];
                packing_list_details = details.id;
            }
        } catch (error) {
            console.log(error.response);
        }
        return packing_list_details;
    }

    async function __getSlotDetails(slotId) {
        let slotDetail = "";
        try {
            const key = "DailyScanningSlot";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "seq_no",
                    "operator": "=",
                    "value": slotId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].DailyScanningSlot.length > 0) {
                const details = getDetails[0].DailyScanningSlot[0];
                slotDetail = details.seq_no;
            }
        } catch (error) {
            console.log(error.response);
        }
        return slotDetail;
    }


    async function __getBoxDetails(boxId){
        // console.log(boxId);
        let boxDetail = "";
        try {
            const key = "PackingListDetail";
            const distinct = false;
            const select = ["*"];
            const where = [
                {
                    "field-name": "id",
                    "operator": "=",
                    "value": boxId
                }
            ];
            const relations = [];
            const orderby = "id:desc";
            const limit = 1;

            const getDetails = await __getDetails(key, distinct, select, where, relations, orderby, limit);

            if (getDetails && getDetails !== "Error" && getDetails[0].PackingListDetail.length > 0) {
                const details = getDetails[0].PackingListDetail[0];
                scannedPackingListDetails = [];
                scannedPackingListDetails.push(details);
                boxDetail = details.id;
            }
        } catch (error) {
            console.log(error.response);
        }
        return boxDetail;
    }

    /*********************************************************/
    /********      Framework Public Functions       **********/

    /*********************************************************/

    async function handleResetData() {
        let today = new Date();
        handleChangeDate(today);
    }

    function handleChangeDate(date) {
        console.log("HANDLE CHAGE DATE CALLED");
        config["inputDate"].setDate(__formatDateYmd(date));
        __resetForm();
        document.getElementById("inputShift").focus();
    }

    async function handleScanShift(event) {
        console.log(new Date());
        event.preventDefault();
        try {
            const shiftId = config["inputShift"].data.value;
            const shiftDetail = await __getShiftDetails(shiftId);

            if (shiftDetail !== "") {
                config["inputShiftId"].setValue(shiftId);
                config["inputShiftName"].setValue(shiftDetail);
                config["inputShift"].setValue(" ");
                document.getElementById("inputTeam").focus();
            } else {
                config["inputShiftId"].setValue("");
                config["inputShiftName"].setValue("");
                config["inputShift"].setValue(" ");
                document.getElementById("inputShift").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Shift Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanTeam(event) {
        event.preventDefault();
        let today = new Date();
        let hours = today.getHours();
        let minutes = today.getMinutes();
        
        try {
            const teamId = config["inputTeam"].data.value;
            const teamDetail = await __getTeamDetails(teamId);
            const slotDetails = await __getAutoSlotDetails();

            if (teamDetail !== "") {
                config["inputTeamId"].setValue(teamId);
                config["inputTeamName"].setValue(teamDetail);
                config["inputTeam"].setValue(" ");
                document.getElementById("inputSlot").focus();
            } else {
                config["inputTeamId"].setValue("");
                config["inputTeamName"].setValue("");
                config["inputTeam"].setValue(" ");
                document.getElementById("inputTeam").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Team Id", "");
            }

            if(slotDetails.data){
                console.log(slotDetails.data , "SLOT DETAILS");
                config["inputSlot"].setValue(slotDetails.data.seq_no);
            }

            if(config["inputSlot"].data.value !== "" && config["inputSlot"].data.value !== ""){
                handleScanSlot(event);
            }

            

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanPackingList(event){
        event.preventDefault();
        try {
            const packingListId = config["inputPackingList"].data.value;
            const packingListDetails = await __getPackinglistDetails(packingListId);

            if (packingListDetails !== "") {
                config["inputPackingListId"].setValue(packingListId);
                config["inputPackingListName"].setValue(packingListDetails);
                config["inputPackingList"].setValue(" ");
                document.getElementById("inputSlot").focus();
            } else {
                config["inputPackingListId"].setValue("");
                config["inputPackingListName"].setValue("");
                config["inputPackingList"].setValue(" ");
                document.getElementById("inputPackingList").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Packing List Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleScanSlot(event) {
        event.preventDefault();
        try {
            const slotId = config["inputSlot"].data.value;
            const slotDetail = await __getSlotDetails(slotId);

            if (slotDetail !== "") {
                config["inputSlotId"].setValue(slotId);
                config["inputSlotName"].setValue(slotDetail);
                config["inputSlot"].setValue(" ");
                handleSetScanningSlot(event);
                // document.getElementsByClassName("main-button")[0].click();
            } else {
                config["inputSlotId"].setValue("");
                config["inputSlotName"].setValue("");
                config["inputSlot"].setValue(" ");
                document.getElementById("inputSlot").focus();
                config["CONTROL_CENTER"].promptWarningMessage("Invalid Slot Id", "");
            }

        } catch (error) {
            console.log(error.response);
        }
    }

    async function handleSetScanningSlot(event) {
        event.preventDefault();
        try {
            const date = config["inputDate"].data.value;
            const shiftId = config["inputShiftId"].data.value;
            const shiftName = config["inputShiftName"].data.value;
            const teamId = config["inputTeamId"].data.value;
            const packing_list_id = config["inputPackingListId"].data.value;
            const teamName = config["inputTeamName"].data.value;
            const slotId = config["inputSlotId"].data.value;
            let dailyScanningSlotId = "";
            let dailyShiftTeamId = "";

            if(date !== "" && shiftId !== "" && teamId !== "" && slotId !== ""){
                const apiRequest = {
                    "current_date": __formatDateYmd(date),
                    "shift_id": shiftId,
                    "team_id": teamId,
                    "seq_no": slotId
                };
                
                document.getElementById("spinner").style.display = "";
    
                const setScanningSlot = await API.post(`dailyScanningSlots/getBySeqNo`, apiRequest);

                console.log(apiRequest);
                console.log(setScanningSlot);

                document.getElementById("spinner").style.display = "none";
    
                if((setScanningSlot.data.DailyScanningSlot !== null) && (setScanningSlot.data.DailyShiftTeam !== null)){
                    console.log('TREEEEE');
                    dailyScanningSlotId = setScanningSlot.data.DailyScanningSlot.id;
                    dailyShiftTeamId = setScanningSlot.data.DailyShiftTeam.id;
                    
                    setScanDate(__formatDateYmd(date));
                    setScanShift(shiftName);
                    setScanTeam(teamName);
                    setScanPackingList(packing_list_id);
                    setScanSlot(slotId);
                    setScanId(dailyScanningSlotId);
                    setDailyShiftTeamId(dailyShiftTeamId);

                    // history.push(`/scanBundle`);
                    // window.location.reload();
                    dailyScanningSlotIdX = dailyScanningSlotId;
                    dailyShiftTeamIdX = dailyShiftTeamId;
                    __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                }else{
                    config["CONTROL_CENTER"].promptWarningMessage("No Data Available", "");
                }

            }else{
                if(date === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Select Date", "");
                }else if(shiftId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Shift", "");
                }else if(teamId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Team", "");
                }else if(slotId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Slot", "");
                }
            }
            
        } catch (error) {
            document.getElementById("spinner").style.display = "none";
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    
    async function handleScanBundleTicket() {
        try {
            const bundleTicketId = config["inputBundleId"].data.value;
            const dailyScanningSlotId = dailyScanningSlotIdX;
            const dailyShiftTeamId = dailyShiftTeamIdX;
            var packing_list_id = null;

            const date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            let cDate = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;


            if(bundleTicketId !== "" && dailyScanningSlotId !== ""){
                const user = getUser();
                const apiRequest = {
                    "bundle_ticket_id": bundleTicketId,
                    "daily_scanning_slot_id": dailyScanningSlotId,
                    "daily_shift_team_id" : dailyShiftTeamId,
                    "packing_list_id":packing_list_id,
                    "scan_date_time":cDate,
                    "user_id": user.name
                };

                console.log(JSON.stringify(apiRequest));

                document.getElementById("spinner").style.display = "";

                const scanBundleTicket = await API.post(`bundleTickets/scanByScanningSlot`, apiRequest);

                document.getElementById("spinner").style.display = "none";

                if (scanBundleTicket.data.status === "success") {
                    config["CONTROL_CENTER"].promptBaseMessage("Records Updated Successfully", "");
                    await __setScanningList(dailyScanningSlotId, dailyShiftTeamId);
                    document.getElementById("inputBundleId").focus();
                } else {
                    config["CONTROL_CENTER"].promptWarningMessage("Error", "");
                }

                config["inputBundleId"].setValue("");

            }else{
                if(bundleTicketId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Enter Barcode", "");
                }else if(dailyScanningSlotId === ""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Select Scanning Slot", "");
                }else if(packing_list_id ===""){
                    config["CONTROL_CENTER"].promptWarningMessage("Please Scan Packing List", "");
                }
            }

            
        } catch (error) {
            console.log(error);
            document.getElementById("spinner").style.display = "none";
            config["inputBundleId"].setValue("");
            document.getElementById("inputBundleId").focus();
            try {
                if (error.response.data.message) {
                    try {
                        let errors = [];

                        Object.entries(JSON.parse(error.response.data.message)).forEach(([index, data]) => {
                            data.forEach(error => errors.push(error));
                        });

                        config["CONTROL_CENTER"].promptWarningMessage(errors[0], "");
                    } catch (error) {
                        config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
                    }
                }
            } catch (error) {
                config["CONTROL_CENTER"].promptErrorMessage("Error", "Please Contact System Administrator");
            }
        }
    }

    function onChange(event) {
        event.preventDefault();
        alert("This is the place where you write CHANGE")
    }

    function onPopulate(event) {
        event.preventDefault();
        alert("This is the place where you write POPULATE")
    }

    function onNew() {
        let dataArray = {};
        //Action handling when NEW buttion clicked...
        alert("This is the place where you write NEW")
        return dataArray
    }

    function onDelete() {
        //Action handling when DELETE buttion clicked...
        alert("This is the place where you write DELETE")
    }

    function onRefresh() {
        //Action handling when REFRESH buttion clicked...
        alert("This is the place where you write REFRESH")
    }

    function onSaveNew(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveModify(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    function onSaveDelete(dataArr) {
        let resultArr = {}
        //Your code goes here...
        return resultArr
    }

    return generateSetScanningSlotDisplay(config)
}

export default SetScanningSlot;