
let componentListConfig = []

componentListConfig["CONTROL_CENTER"] = {
    objectType: "Controller",
    schema: {
        id: "formSetScanningSlot",
        name: "formSetScanningSlot",
        controllerObject: componentListConfig,
        create: true,
        createAPI: "",
        read: true,
        readAPI: "",
        update: true,
        updateAPI: "",
        delete: false,
        deleteAPI: ""
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSetScanningSlot",
            type: "text",
            visible: true,
            value: "Set Scanning Slot"
        },
    },
    state: {
        populated: false,
        modified: false,
        deleted: false,
        new: false
    },
    actions: {
        save: "buttonSave",
        delete: "buttonDelete",
        populate: "buttonPopulate",
        refresh: "buttonRefresh"
    },
    event: {
    }
}

componentListConfig["buttonPopulate"] = {
    objectType: "Button",
    schema: {
        id: "buttonPopulate",
        name: "buttonPopulate",
        type: "submit",
        label: "Populate",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonNew"] = {
    objectType: "Button",
    schema: {
        id: "buttonNew",
        name: "buttonNew",
        type: "submit",
        label: "New",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonSave"] = {
    objectType: "Button",
    schema: {
        id: "buttonSave",
        name: "buttonSave",
        type: "submit",
        label: "Save",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonRefresh"] = {
    objectType: "Button",
    schema: {
        id: "buttonRefresh",
        name: "buttonRefresh",
        type: "submit",
        label: "Clear",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["buttonDelete"] = {
    objectType: "Button",
    schema: {
        id: "buttonDelete",
        name: "buttonDelete",
        type: "submit",
        label: "Delete",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputDate"] = {
    objectType: "DateField",
    schema: {
        name: "inputDate",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelInputDate",
            type: "text",
            visible: true,
            value: "Date"
        },
        class: ""
    },
    data: {
        sqlcolumn: "date",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShift"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputShift",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShift",
            type: "text",
            visible: true,
            value: "Shift"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftId",
        placeholder: "Shift Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftId",
            type: "text",
            visible: false,
            value: "Shift Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputShiftName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputShiftName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelShiftName",
            type: "text",
            visible: true,
            value: "Shift Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "shift_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["newCheckBox"] = {
    objectType: "CheckBox",
    schema: {
        id : "newCheckBox",
        name: "newCheckBox",
        placeholder: "",
        type: "checkbox",
        label: "Change Quantity",
        checkedValue: "1",
        uncheckedValue: "0",
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelNewCheckBox",
            type: "text",
            visible: true,
            value: "New  Box"
        },
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["newInputButton"] = {
    objectType: "Button",
    schema: {
        id: "newInputButton",
        name: "newInputButton",
        type: "submit",
        label: "Enter",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["inputSecondPackingList"] = {
    objectType: "DropDown",
    schema: {
        id : "inputSecondPackingList",
        name: "inputSecondPackingList",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanPackingList",
            type: "text",
            visible: true,
            value: "Packing List"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    options: [{ value: "", text: "- Select Packing List -" }],
    class: "",
    event: {}
}

componentListConfig["viewOriginalQty"] = {
    objectType: "TextBox",
    schema: {
        name: "viewOriginalQty",
        placeholder: "Original Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelOriginalQty",
            type: "text",
            visible: true,
            value: "original Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: 0
    },
    class: "",
    event: {}
}

componentListConfig["viewScanedQty"] = {
    objectType: "TextBox",
    schema: {
        name: "viewScanedQty",
        placeholder: "Scaned Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelScanedQty",
            type: "text",
            visible: true,
            value: "Scanned Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}


let grid1Cols = [];

grid1Cols["Secondary_ID"] = { objectType: "IntegerField", colIndex: 0, datatype: "text", name: "Secondary_ID", placeholder: "Secondary ID", visible: true, editable: false, sqlColumn: "Secondary_ID", style: { textAlign: "left", minWidth: "150px", width: "150px"  } };
grid1Cols["Bundle_ID"] = { objectType: "IntegerField", colIndex: 1, datatype: "text", name: "Bundle_ID", placeholder: "Bundle ID", visible: true, editable: false, sqlColumn: "Bundle_ID", style: { textAlign: "left", minWidth: "150px", width: "150px"  } };
grid1Cols["Ticket_ID"] = { objectType: "IntegerField", colIndex: 2, datatype: "text", name: "Ticket_ID", placeholder: "Ticket ID", visible: true, editable: false, sqlColumn: "Ticket_ID", style: { textAlign: "left", minWidth: "150px", width: "150px"  } };
grid1Cols["Original_Qty"] = { objectType: "IntegerField", colIndex: 3, datatype: "text", name: "Original_Qty", placeholder: "Original Qty", editable: false, sqlColumn: "Original_Qty", style: { textAlign: "left", minWidth: "150px", width: "150px" } }
grid1Cols["Scanned_Qty"] = { objectType: "IntegerField", colIndex: 4, datatype: "text", name: "Scanned_Qty", placeholder: "Scanned Qty", editable: true, sqlColumn: "Scanned_Qty", style: { textAlign: "left", minWidth: "150px", width: "150px" } }
grid1Cols["FPO"] = { objectType: "TextBox", colIndex: 5, datatype: "text", name: "FPO", placeholder: "FPO", editable: false, sqlColumn: "FPO", style: { textAlign: "left", minWidth: "150px", width: "150px" } };
grid1Cols["Operation"] = { objectType: "TextBox", colIndex: 6, datatype: "text", name: "Operation", placeholder: "Operation", editable: false, sqlColumn: "Operation", style: { textAlign: "left", minWidth: "150px", width: "150px" } };
grid1Cols["Size"] = { objectType: "TextBox", colIndex: 7, datatype: "text", name: "Size", placeholder: "Size", editable: false, sqlColumn: "Size", style: { textAlign: "left", minWidth: "150px", width: "150px" } };
grid1Cols["Team"] = { objectType: "IntegerField", colIndex: 8, datatype: "text", name: "Team", placeholder: "Team", editable: false, sqlColumn: "Team", style: { textAlign: "left", minWidth: "150px", width: "150px" } };
grid1Cols["Slot"] = { objectType: "IntegerField", colIndex: 9, datatype: "text", name: "Slot", placeholder: "Slot", visible: true, editable: false, sqlColumn: "Slot", style: { textAlign: "left", minWidth: "150px", width: "150px" } }
grid1Cols["Action"] = { objectType: "TextBox", colIndex: 10, datatype: "text", name: "Action", placeholder: "Action", visible: true, editable: false, sqlColumn: "Action", style: { textAlign: "left", minWidth: "150px", width: "150px" } }

componentListConfig["gridData"] = {
    objectType: "Grid",
    schema:{
        name: "gridData",
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    controller: {
        id: "gridData",
        name: "gridData",
        descriptoin: "Data",
        type: "table",
        keyField: "id",
        visible: true,
        Create: true,
        CreateAPI: "",
        Read: true,
        ReadAPI: "",
        Update: true,
        UpdateAPI: "",
        delete: false,
        DeleteAPI: "",
        State: {
            Populated: false,
            Modified: false
        },
        Actions: {
            Save: "",
            Delete: "",
            Read: ""
        }
    },
    defaultRowCount: 1,
    columns: grid1Cols,
    data: [],
    event: {}
}

componentListConfig["inputScannedQty"] = {
    objectType: "TextBox",
    schema: {
        id: "inputScannedQty",
        name: "inputScannedQty",
        placeholder: "Enter Scanned Qty",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: false,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelOriginalQty",
            type: "text",
            visible: true,
            value: "New Scanned Quantity"
        },
        class: ""
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeam"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputTeam",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeam",
            type: "text",
            visible: true,
            value: "Team"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamId",
        placeholder: "Team Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamId",
            type: "text",
            visible: false,
            value: "Team Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTeamName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTeamName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTeamName",
            type: "text",
            visible: true,
            value: "Team Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "team_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBundleId"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputBundleId",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBundleId",
            type: "text",
            visible: true,
            value: "Bundle Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "bundle_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}



// /////////////////////////////////

componentListConfig["inputBox"] = {
    objectType: "IntegerField",
    schema: {
        id : "inputBox",
        name: "inputBox",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBox",
            type: "text",
            visible: true,
            value: "Box ID"
        },
        class: ""
    },
    data: {
        sqlcolumn: "box_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBoxId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputBoxId",
        placeholder: "Box Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBoxId",
            type: "text",
            visible: false,
            value: "Box Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "box_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputBoxName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputBoxName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelBoxName",
            type: "text",
            visible: true,
            value: "Box Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "box_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTicket"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputTicket",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTicket",
            type: "text",
            visible: true,
            value: "Ticket ID"
        },
        class: ""
    },
    data: {
        sqlcolumn: "ticket_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTicketId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTicketId",
        placeholder: "Ticket Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTicketId",
            type: "text",
            visible: false,
            value: "Ticket Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "ticket_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputTicketName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputTicketName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelTicketName",
            type: "text",
            visible: true,
            value: "Ticket Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "ticket_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["quantityModifier"] = {
    objectType: "QuantityModifier",
    schema:{
        name: "quantityModifier",
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    data: {
        sqlcolumn: "",
        oldValue: "",
        value: "",
        quantityList: [],
    },
    event:{}
}




/////////////////////////////////////////  Packing List  ////////////////////////////////////////////////////////

componentListConfig["inputPackingList"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputPackingList",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingList",
            type: "text",
            visible: true,
            value: "Packing List"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputPackingListId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputPackingListId",
        placeholder: "Packing List Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingListId",
            type: "text",
            visible: false,
            value: "Packing List Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputPackingListName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputPackingListName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelinputPackingListName",
            type: "text",
            visible: true,
            value: "Packing List Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "packing_list_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

/////////////////////////////////////////////////////////////////////////////////////////////////

componentListConfig["inputSlot"] = {
    objectType: "IntegerField",
    schema: {
        name: "inputSlot",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlot",
            type: "text",
            visible: true,
            value: "Slot"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotId"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotId",
        placeholder: "Slot Id",
        type: "text",
        length: 100,
        showLabel: true,
        visible: false,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotId",
            type: "text",
            visible: false,
            value: "Slot Id"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_id",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["inputSlotName"] = {
    objectType: "TextBox",
    schema: {
        name: "inputSlotName",
        placeholder: "",
        type: "text",
        length: 100,
        showLabel: true,
        visible: true,
        insertable: true,
        updateAllowed: true,
        mandetory: true,
        searchable: true,
        readOnly: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    label: {
        objectType: "Label",
        schema: {
            name: "labelSlotName",
            type: "text",
            visible: true,
            value: "Slot Name"
        },
        class: ""
    },
    data: {
        sqlcolumn: "slot_name",
        oldValue: "",
        value: ""
    },
    class: "",
    event: {}
}

componentListConfig["okButton"] = {
    objectType: "Button",
    schema: {
        id: "okButton",
        name: "okButton",
        type: "submit",
        label: "OK",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

componentListConfig["resetButton"] = {
    objectType: "Button",
    schema: {
        id: "resetButton",
        name: "resetButton",
        type: "submit",
        label: "Reset",
        disabled: false,
        visible: true,
        dataSourceController: componentListConfig["CONTROL_CENTER"]
    },
    event: {}
}

export default componentListConfig