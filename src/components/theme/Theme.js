import React from 'react';

const Theme = ({ header: Header, main: Main, footer: Footer }) => {
    return (
        <>
            <Header />
            <div className="content-wrapper">
                <div className="container-fluid content container-background">
                    <div className="row justify-content-center">
                        <div className="col-12">
                            <Main />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default Theme;