import React from 'react';
import { getUser, removeUserSession, removeScanDate, removeScanShift, removeScanTeam, removeScanSlot, removeScanId, removeDailyShiftTeamId } from '../../utils/Common';
import { useHistory } from 'react-router-dom';
import API from '../../api/API';
import Logo from "../../images/logo.jpg";
const Header = () => {
    const user = getUser();
    const history = useHistory();
    const handleLogout = () => {
        API.post('logout').then(response => {
            console.log("************response.data**********");
            console.log(response.data);
            removeUserSession();
            removeScanDate();
            removeScanShift();
            removeScanTeam();
            removeScanSlot();
            removeScanId();
            removeDailyShiftTeamId();
            history.push('/login');
            window.location.reload();
        }).catch(error => {
            console.log("************error.response**********");
            console.log(error.response);
            removeUserSession();
        });
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top navbar-background">
            <div className="container">
                <img src={Logo} />
                {/* <a className="navbar-brand" href={`${process.env.PUBLIC_URL}/setScanningSlot`}>
                    <span>Bundle Scanning</span></a> */}

                <a className="navbar-brand" href={`${process.env.PUBLIC_URL}/setScanningSlot`}>
                <span>Bundle Scanning</span></a>                    

                <a className="navbar-brand" href={`${process.env.PUBLIC_URL}/fgScanning`}>
                    <span>FG Scanning</span></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto mr-0 my-2 my-md-0">
                        {
                            user && (
                                <>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="userDropdown" href="#!" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fas fa-user fa-fw"></i>&nbsp;{user.name}</a>
                                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" onClick={handleLogout}>Logout</a>
                                        </div>
                                    </li>
                                </>
                            )
                        }
                    </ul>
                </div>
            </div>
        </nav>
    );
}
export default Header;
