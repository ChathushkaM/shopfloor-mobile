import React, { useState } from 'react';
import { setUserSession, removeUserSession, removeScanDate, removeScanShift, removeScanTeam, removeScanSlot, removeScanId, removeDailyShiftTeamId } from '../utils/Common';
import API from '../api/API';
import logo from '../images/logo_.png';

const Login = ({ ...props }) => {
  const [loading, setLoading] = useState(false);
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);

    API.post('login', { email: email.value, password: password.value }).then(response => {
      setLoading(false);
      const user = { name: email.value }
      setUserSession(response.data.access_token, user);
      removeScanDate();
      removeScanShift();
      removeScanTeam();
      removeScanSlot();
      removeScanId();
      removeDailyShiftTeamId();
      props.history.push(`/setScanningSlot`);
      window.location.reload();
    }).catch(error => {
      setLoading(false);
      setError("You have entered an invalid email or password");
      removeUserSession();
    });
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-5">
          <div className="card shadow-lg border-0 my-5 login-card">
            <div className="card-header">
            </div>
            <div className="card-body">
              <form>
                {error && <div className="alert alert-danger" role="alert">{error}</div>}
                <div className="form-group d-flex align-items-center justify-content-center mt-4 mb-0">
                  <img src={logo} alt="Logo" style={{ width: "100px" }} />
                </div>
                <div className="form-group">
                  <label className="mb-1" htmlFor="inputEmailAddress">Username</label>
                  <input className="form-control" id="inputEmailAddress" type="email" name="email" placeholder="Enter email address" required autoFocus {...email} />
                </div>
                <div className="form-group">
                  <label className="mb-1" htmlFor="inputPassword">Password</label>
                  <input className="form-control" id="inputPassword" type="password" name="password" placeholder="Enter password" required {...password} />
                </div>
                <div className="form-group d-flex align-items-center justify-content-center mt-4 mb-0">
                  <button className="btn btn-lg" type="submit" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} style={{ backgroundColor: "#064179", color: "white" }}>Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Login;