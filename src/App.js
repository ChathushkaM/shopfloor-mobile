import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PrivateRoute from './utils/PrivateRoute';

//Theme
import Theme from './components/theme/Theme';

//Public Routes
import Login from './components/Login';

//Private Routes
import Dashboard from './components/pages/dashboard/DashboardES';
import Sample from './components/pages/Sample/SampleES';
import Home from './components/pages/Home/HomeES';
import SetScanningSlot from './components/pages/SetScanningSlotNew/SetScanningSlotES';
import ScanBundle from './components/pages/ScanBundleNew/ScanBundleES';
import FgScanning from './components/pages/FgScan/FgScanES';
import SetScanningSlotOld from './components/pages/OldSetScanningSlot/SetScanningSlotES';
import ScanBundleOld from './components/pages/OldScanBundle/ScanBundleES';


const getBaseName = path => path.substr(0, path.lastIndexOf('/'));

const App = () => {
  return (
    <>
      <Router basename={getBaseName(window.location.pathname)}>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/login" component={Login} />
          <PrivateRoute path="/dashboard" component={Dashboard} theme={Theme} />
          <PrivateRoute path="/sample" component={Sample} theme={Theme} />
          <PrivateRoute path="/home" component={Home} theme={Theme} />
          <PrivateRoute path="/setScanningSlot" component={SetScanningSlotOld} theme={Theme} />
          <PrivateRoute path="/scanBundle" component={ScanBundleOld} theme={Theme} />
          <PrivateRoute path="/fgScanning" component={FgScanning} theme={Theme} />
          {/* <PrivateRoute path="/setScanningSlotOld" component={SetScanningSlotOld} theme={Theme} /> */}
          {/* <PrivateRoute path="/scanBundleOld" component={ScanBundleOld} theme={Theme} /> */}
        </Switch>
      </Router>
    </>
  );
}

export default App;